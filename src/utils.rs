/// EnsureAny is an iterator which clears the collection
/// if the predicate is not met
#[derive(Clone)]
pub struct EnsureAny<I: Clone, P> {
    iter: I,
    predicate: P,
    does_predicate_match: Option<bool>,
}

impl<I: Iterator + Clone, P> Iterator for EnsureAny<I, P>
    where P: FnMut(&I::Item) -> bool,
{
    type Item = I::Item;

    fn next(&mut self) -> Option<I::Item> {
        if self.does_predicate_match.is_none() {
            let found = self.iter.clone().find(&mut self.predicate);
            self.does_predicate_match = Some(found.is_some());
        }
        if self.does_predicate_match.unwrap() == true {
            self.iter.next()
        } else {
            None
        }
    }
}

pub trait EnsureAnyTrait: Sized + Iterator + Clone {
    fn ensure_any<P>(self, predicate: P) -> EnsureAny<Self, P> 
        where P: FnMut(&Self::Item) -> bool;
}

impl<I: Iterator + Clone> EnsureAnyTrait for I {
    fn ensure_any<P>(self, predicate: P) -> EnsureAny<I, P> 
        where P: FnMut(&I::Item) -> bool,
    {
        EnsureAny { iter: self, predicate, does_predicate_match: None }
    }
}

// macro to cast an enum into an enum variant
macro_rules! cast_enum {
    ($target: expr, $pat: path) => {
        {
            if let $pat(a) = $target {
                a
            } else {
                panic!(
                    "mismatch variant when cast to {}", 
                    stringify!($pat));
            }
        }
    };
}

// macro_rules! try_cast_enum {
//     ($target: expr, $pat: path) => {
//         {
//             if let $pat(a) = $target {
//                 Some(a)
//             } else {
//                 None
//             }
//         }
//     };
// }