mod orchestrator_fsm;
use orchestrator_fsm::*;

use crate::{model::*};

// IMPORTANT NOTE: this is an assumed anti-pattern. 
// Using mutable static variables is only meant to achieve
// a temporary codebase in which the orchestrator is seen
// as a SERVICE ENDPOINT by the console.
// In a real-world use case, this code would be on a server, backed-up
// with a database, and the client would call it through web services.
// Typically, the battlefield state between game turns, seen as the
// "source of truth", cannot ever be provided TO the backend / server 
// FROM the frontend / client without enabling a player to cheat.
// Regardless of the above, adding a json file, a database
// or any other persistence tool whose concurrency safety is enforced
// outside of the scope of this Rust programme could be considered.
// Static mutable fields will be used in the meanwhile.

// inv: always holds a value once initialised (i.e., orchestrator::start_battle)
static mut STATE: Option<OrchestratorState> = None;

pub fn start_battle(battlefield: Battlefield) {
    unsafe { 
        STATE = Some(OrchestratorState::new(battlefield));
    }
}

pub fn unregister_monster_action(monster_id: MonsterId) {
    unsafe {
        let state = STATE
            .clone()
            .expect("uninitialised (start_battle)")
            .do_transition(OrchestratorEvent::UnregisterAction { caster_id: monster_id });
        STATE = Some(state);
    }
}

pub fn register_monster_move(move_action: MoveAction) {
    unsafe {
        let state = STATE
            .clone()
            .expect("uninitialised (start_battle)")
            .do_transition(OrchestratorEvent::RegisterMoveAction { move_action });
        STATE = Some(state);
    }
}

pub fn register_monster_swap(swap_action: SwapAction) {
    unsafe {
        let state = STATE
            .clone()
            .expect("uninitialised (start_battle)")
            .do_transition(OrchestratorEvent::RegisterSwapAction { swap_action });
        STATE = Some(state);
    }
}

pub fn lock_action_registrations() -> LastTurnLog {
    unsafe {
        let state = STATE
            .clone()
            .expect("uninitialised (start_battle)")
            .do_transition(OrchestratorEvent::LockActionRegistrations);
        let result = state.last_turn_log().clone().unwrap();
        STATE = Some(state);
        result
    }
}

pub fn register_replacement(swap_action: SwapAction) {
    unsafe {
        let state = STATE
            .clone()
            .expect("uninitialised (start_battle)")
            .do_transition(OrchestratorEvent::RegisterReplacement { swap_action });
        STATE = Some(state);
    }
}

pub fn unregister_replacement(outgoing_id: MonsterId) {
    unsafe {
        let state = STATE
            .clone()
            .expect("uninitialised (start_battle)")
            .do_transition(OrchestratorEvent::UnregisterReplacement { knocked_out: outgoing_id });
        STATE = Some(state);
    }
}

pub fn lock_replacement_registrations() -> LastTurnLog {
    unsafe {
        let state = STATE
            .clone()
            .expect("uninitialised (start_battle)")
            .do_transition(OrchestratorEvent::LockReplacementRegistrations);
        let result = state.last_turn_log().clone().unwrap();
        STATE = Some(state);
        result
    }
}

pub fn get_eligible_swap_incoming_monsters(outgoing: MonsterId) -> Vec<MonsterId> {
    unsafe {
        let state = STATE
            .clone()
            .expect("uninitialised (start_battle)");
        let battlefield = state.battlefield();
        // Note that pending_incomings do not contain incoming monster_ids
        // which are already registered for a swap with the given outgoing,
        // i.e., if a swap is registered between A and B, B is still a valid incoming for an outgoing A
        let pending_incomings = match state.state_enum() {
            OrchestratorStateEnum::ActionRegistration { pending_actions } => pending_actions
                .iter()
                .filter_map(|action| match action {
                    Action::Swap(swap_action) => 
                        if swap_action.get_outgoing() == outgoing { 
                            None 
                        } else {
                            Some(swap_action.get_incoming())
                        },
                    _ => None,
                })
                .collect::<Vec<MonsterId>>(),
            OrchestratorStateEnum::ReplacementRegistration { pending_replacements } => pending_replacements
                .iter()
                .filter_map(|&(_, replacement_opt)| replacement_opt)
                .collect::<Vec<MonsterId>>(),
        };
        let eligible_incomings = battlefield.get_party_player_monsters()
            .into_iter()
            .filter_map(|monster| {
                let initial_condition = true
                    && !monster.is_ko() 
                    && !pending_incomings.contains(&battlefield.get_monster_id(monster).unwrap())
                ;
                initial_condition.then(|| battlefield.get_monster_id(monster).unwrap())
            })
            .collect();
        eligible_incomings
    }
}

pub fn get_eligible_move_target_monsters(caster: MonsterId, move_offset: u8) -> Vec<MonsterId> {
    unsafe {
        let state = STATE
            .clone()
            .expect("uninitialised (start_battle)");
        let battlefield = state.battlefield();
        let result = match state.state_enum() {
            OrchestratorStateEnum::ReplacementRegistration {..} => Vec::new(),
            OrchestratorStateEnum::ActionRegistration {..} => battlefield
                .get_field_oppont_monsters()
                .into_iter()
                .filter_map(|potentiel_target| (!potentiel_target.is_ko()).then(|| battlefield.get_monster_id(potentiel_target).unwrap()))
                .collect()
        };
        result
    }
}
