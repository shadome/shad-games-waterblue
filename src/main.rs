use model::*;

#[macro_use]
mod utils;

mod battlecore;
mod console;
mod model;
mod orchestrator;
mod robot;

#[allow(dead_code)]
fn main() -> Result<(), Box<dyn std::error::Error>> {

    // Beginner's note: &* recreates a reference from &mut T to &T
    let level = Level::new(50).unwrap();
    let player_monsters = vec![
        // Monster
        //     ::new(Species::N025_PIKACHU, level)
        //     .set_status(Some(StatusCondition::Infatuation))
        //     .set_cur_hpt_from_cur_hpt(|cur| cur - 15)
        // ,
        // Monster
        //     ::new(Species::N007_SQUIRTLE, level)
        //     .set_status(Some(StatusCondition::Freeze))
        //     .set_cur_hpt_from_max_hpt(|max| max / 2 - 12)
        // ,
        // Monster::new(Species::N094_GENGAR, level),
        // Monster::new(Species::N065_ALAKAZAM, level),
        Monster::new(Species::N001_BULBASAUR, level),
        Monster::new(Species::N025_PIKACHU, level),
        Monster::new(Species::N007_SQUIRTLE, level),
        Monster::new(Species::N004_CHARMANDER, level),
    ];
    let oppont_monsters = vec![
        Monster::new(Species::N094_GENGAR, level),
        Monster::new(Species::N094_GENGAR, level),
        // Monster::new(Species::N004_CHARMANDER, level),
            // .set_status(Some(StatusCondition::Toxic))
            // .set_cur_hpt_from_max_hpt(|max| max - 12)
        Monster::new(Species::N065_ALAKAZAM, level),
        Monster::new(Species::N001_BULBASAUR, level),
        Monster::new(Species::N025_PIKACHU, level),
        Monster::new(Species::N007_SQUIRTLE, level),
        // Monster
        //     ::new(Species::N092_GASTLY, Level::new(1).unwrap())
        //     .set_cur_hpt(0)
        // ,
        // Monster
        //     ::new(Species::N063_ABRA, Level::new(1).unwrap())
        //     .set_cur_hpt(0)
        // ,
    ];
    let battlefield = Battlefield::new(
        player_monsters,
        oppont_monsters,
    )?;
    orchestrator::start_battle(battlefield.clone());
    return console::run(&battlefield);
}




// /* Real external implementation (let's say it's another crate's function) */
// pub fn get_data_external(payload: String) -> Option<String> {
//     return Some(payload); // would do something great
// }

// /* Client-side abstraction through a trait */
// pub trait TBackendDependency {
//     fn get_data(&self, payload: String) -> Option<String>;
// }

// /* Client-side release code */
// pub struct BackendDependency {}

// impl BackendDependency {
//     pub fn new() -> Self { Self {} }
// }

// impl TBackendDependency for BackendDependency {
//     fn get_data(&self, payload: String) -> Option<String> {
//         get_data_external(payload)
//     }
// }

// /* Client-side test code */
// #[cfg(test)]
// mod test {
//     use super::*;

//     pub struct BackendDependencyForTesting<FnGetData> where FnGetData: FnMut(String) -> Option<String> {
//         get_data_callback: FnGetData,
//     }

//     impl<FnGetData> BackendDependencyForTesting<FnGetData> where FnGetData: FnMut(String) -> Option<String> {
//         pub fn new(
//             callback: FnGetData,
//             // callback: Box<(dyn Fn(String) + 'static)>,
//         ) -> Self { 
//             Self { get_data_callback: callback } 
//         }
//     }

//     impl<FnGetData> TBackendDependency 
//         for BackendDependencyForTesting<FnGetData> 
//         where FnGetData: FnMut(String) -> Option<String>
//     {
//         fn get_data(&self, payload: String) -> Option<String> {
//             (self.get_data_callback)(payload)
//         }
//     }

//     #[test]
//     fn test() {
//         let mut nb_of_calls = 0;
//         let callback = |string: String| {
//             // nb_of_calls += 1; // <-- this will not work, requires a FnMut, not a Fn
//             Some(string) 
//         };
//         let backend_dependency_for_testing = BackendDependencyForTesting::new(callback);
//         // call the code to test which requires 
//         // an instance of TBackendDependency 
//         // with the aboved mocked implementation
//         // ...
//         assert_eq!(1, nb_of_calls);
//     }
// }