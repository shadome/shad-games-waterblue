mod play_turn;
pub use play_turn::play_turn;

use crate::model::*;

pub(in crate::battlecore) fn calculate_damage(caster: &Monster, target: &Monster, move_power: u8) -> u16 {
    let move_power = move_power as u16;
    let caster_lvl = caster.get_level().as_u16();
    let caster_atk = caster.get_species().get_base_stats().get_atk() as u16;
    let target_def = target.get_species().get_base_stats().get_def() as u16;
    ((2 * caster_lvl / 5) + 2) * move_power * (caster_atk / target_def) / 50 + 2
}
