use crate::model::*;
use rand::Rng;

pub fn get_action(
    bf: &Battlefield, 
    req_caster: &Monster, 
    eligible_incomings: &Vec<MonsterId>,
    eligible_move_targets: &Vec<MonsterId>,
) -> Option<Action> {
    let mut rng = rand::thread_rng();
    let move_offset = rng.gen_range(0, req_caster.get_species().get_moves().len()) as u8;
    let target_id = eligible_move_targets
        .get(rng.gen_range(0, eligible_move_targets.len()))
        .unwrap();
    let move_action = MoveAction::new(
        move_offset,
        bf.get_monster_id(req_caster).unwrap(),
        *target_id,
    );
    Some(Action::Move(move_action))
}

                // let robot_actions = robot::get_actions(
                //     &_self.battlefield, 
                //     /* required_Casters */ &_self.battlefield.get_field_oppont_monsters(), 
                //     /* eligible_targets */ &_self.battlefield.get_field_player_monsters(),
                // );
// pub fn get_actions(
//     battlefield: &Battlefield,
//     required_casters: &Vec<&Monster>,
//     eligible_targets: &Vec<&Monster>,
// ) -> Vec<Action> {
//     let mut rng = rand::thread_rng();
//     required_casters
//         .iter()
//         .map(|monster| {
//             let move_offset = rng.gen_range(0, monster.get_species().get_moves().len()) as u8;
//             let target = eligible_targets
//                 .get(rng.gen_range(0, eligible_targets.len()))
//                 .map(|target_monster| battlefield.get_monster_id(target_monster))
//                 .flatten()
//                 .unwrap();
//             let move_action = MoveAction::new(
//                 move_offset,
//                 battlefield.get_monster_id(monster).unwrap(),
//                 target,
//             );
//             Action::Move(move_action)
//         })
//         .collect()
// }


pub fn get_replacements(
    battlefield: &Battlefield,
    required_replacements: &Vec<MonsterId>,
    eligible_replacements: &Vec<&Monster>,
) -> Vec<SwapAction> {
    let mut rng = rand::thread_rng();
    let mut eligible_replacements = eligible_replacements
        .iter()
        .filter_map(|monster| (!monster.is_ko()).then(|| battlefield.get_monster_id(monster).unwrap()))
        .collect::<Vec<MonsterId>>();
    rand::prelude::SliceRandom::shuffle(&mut eligible_replacements[..], &mut rng);
    required_replacements
        .iter()
        .enumerate()
        .map(|(idx, outgoing)| SwapAction::new(
            /* incoming */ *eligible_replacements.get(idx).unwrap(),
            /* outgoing */ *outgoing,
        ))
        .collect()
}

pub fn get_replacement(
    bf: &Battlefield,
    req_replacement: &MonsterId,
    eligible_replacements: &Vec<MonsterId>,
) -> SwapAction {
    let mut rng = rand::thread_rng();
    let repl_offset = rng.gen_range(0, eligible_replacements.len());
    SwapAction::new(
        eligible_replacements[repl_offset],
        *req_replacement,
    )
}
