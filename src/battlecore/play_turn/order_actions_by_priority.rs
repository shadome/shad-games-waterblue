use crate::{model::*};

pub(super) fn order_actions_by_priority(
    bf: &Battlefield, 
    pending: &Vec<Action>,
) -> Vec<Action> {

    fn get_speed(
        bf: &Battlefield, 
        action: &Action,
    ) -> u8 {
        (*bf)
            .get_monster(action.get_caster_id())
            .unwrap()
            .get_species()
            .get_base_stats()
            .get_spe()
    }
    
    let mut pending = pending.clone();
    pending.sort_by(|action1, action2| match (action1, action2) {
        (Action::Swap { .. }, Action::Move { .. }) => std::cmp::Ordering::Greater,
        (Action::Move { .. }, Action::Swap { .. }) => std::cmp::Ordering::Less,
        (x, y) => get_speed(bf, &y).cmp(&get_speed(bf, &x)),
    });
    pending
}
