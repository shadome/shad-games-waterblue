use crate::{model::*};

pub(super) fn get_monsters_to_replace_for_team(
    bf: &Battlefield, 
    field_monsters: &Vec<&Monster>, 
    party_monsters: &Vec<&Monster>,
) -> Vec<MonsterId> {
    let player_nb_of_eligible_replacements = party_monsters
        .iter()
        .filter(|monster| !monster.is_ko())
        .collect::<Vec<_>>()
        .len();
    let player_knocked_out_monsters_to_replace = field_monsters
        .iter()
        .filter_map(|monster| monster.is_ko().then(|| bf.get_monster_id(monster).unwrap()))
        .take(player_nb_of_eligible_replacements)
        .collect::<Vec<MonsterId>>();
    player_knocked_out_monsters_to_replace
}
