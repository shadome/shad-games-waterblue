use crate::{model::*, utils::*, battlecore};

pub(super) fn apply_action_to_battlefield(
    bf: Battlefield, 
    action: &Action,
) -> Result<ActionLog, Battlefield> { // returns either an action log (containing the updated battlefield) or the parameter battlefield unchanged

    let mut bf = bf;
    match action {
        Action::Move(move_action) => {
            let caster = bf.get_monster(move_action.get_caster_id()).unwrap().clone();
            let mov = caster.get_species().get_moves()[move_action.get_move_offset() as usize];
            let mut mitigation_log = None;
            for effect in mov.get_effects() {
                // mitigation application to one target for one move is short-circuiting
                (bf, mitigation_log) = if mitigation_log.is_some() {
                    (bf, mitigation_log)
                } else {
                    try_apply_immediate_mitigation_to_target(bf, &caster, move_action.get_target_id(), effect)
                };
            }
            if let Some(mitigation) = mitigation_log {
                let log = ActionLog::Move { 
                    action: move_action.clone(), 
                    mitigation: mitigation,
                    updated_battlefield: bf,
                };
                Ok(log)
            } else {
                Err(bf)
            }
        },
        Action::Swap(swap_action) => {
            // TODO check if the outgoing is rooted, cannot go out, etc.

            // Note that those check could also be needed at the action
            // registration, depending on game logic, but the outgoing
            // could still get rooted between registration and this point,
            // e.g., the opponent has just swapped in a "shadow tag" monster
            bf = Battlefield::swap_monsters(bf, swap_action.get_outgoing(), swap_action.get_incoming());
            let log = ActionLog::Swap { 
                action: swap_action.clone(),
                updated_battlefield: bf,
             };
            Ok(log)
        },
    }
}

fn try_apply_immediate_mitigation_to_target(
    bf: Battlefield,
    caster: &Monster,
    target_id: MonsterId,
    effect: Effect,
) -> (Battlefield, Option<MitigationLog>) {
    let target = bf.get_monster(target_id).unwrap();
    let caster_id = bf.get_monster_id(caster).unwrap();
    let is_caster_still_on_field = bf.get_field_monster_ids().iter().any(|&x| x == caster_id);
    if caster.is_ko() || target.is_ko() || !is_caster_still_on_field {
        return (bf, None);
    }
    let damage = effect
        .get_components()
        .iter()
        .ensure_any(|&component| **component == EffectComponent::Immediate)
        .find(|&component| match **component { 
            EffectComponent::Damage(dmg) if dmg.power > 0 => true, 
            _ => false,
        })
        .map(|&dmg_component| battlecore::calculate_damage(
            caster, 
            target, 
            cast_enum!(*dmg_component, EffectComponent::Damage).power,
        ));

    if let Some(damage) = damage {
        let mitigation = Mitigation::Damage(damage);
        let bf = bf.mitigate_monster(target_id, mitigation); // side effect
        let updated_target = bf.get_monster(target_id).unwrap();
        let log = MitigationLog::new(
            target_id, 
            mitigation, 
            Effectiveness::Neutral, 
            /* applied_status */ None, 
            /* is_critical */ false, 
            /* is_lethal */ updated_target.is_ko(),
        );
        (bf, Some(log))
    } else {
        (bf, None)
    }
}
