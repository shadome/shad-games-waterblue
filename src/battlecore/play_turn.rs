mod order_actions_by_priority;
use order_actions_by_priority::*;
mod get_monsters_to_replace_for_team;
use get_monsters_to_replace_for_team::*;
mod apply_action_to_battlefield;
use apply_action_to_battlefield::*;

use crate::model::*;

pub fn play_turn(
    bf: Battlefield, 
    pending: &Vec<Action>,
) -> LastTurnLog {
    let mut action_logs = Vec::new();
    let mut action_log;
    let initial_bf = bf.clone();
    // bf is the most up to date version of the battlefield, updated for each pending action
    let mut bf = bf;
    let sorted_pending = order_actions_by_priority(&bf, pending);
    for pending in sorted_pending {
        action_log = apply_action_to_battlefield(bf, &pending);
        match action_log {
            Ok(log) => {
                bf = log.get_updated_battlefield().clone();
                action_logs.push(log);    
            },
            Err(unchanged_bf) => {
                bf = unchanged_bf;
            }
        }
    }
    let replacements_for_player = get_monsters_to_replace_for_team(&bf, &bf.get_field_player_monsters(), &bf.get_party_player_monsters());
    let replacements_for_oppont = get_monsters_to_replace_for_team(&bf, &bf.get_field_oppont_monsters(), &bf.get_party_oppont_monsters());
    LastTurnLog::new(
        initial_bf,
        action_logs, 
        replacements_for_player, 
        replacements_for_oppont, 
    )
}
