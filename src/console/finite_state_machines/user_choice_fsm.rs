use uuid::Uuid;

use crate::{model::*, orchestrator};

// public

// Copy trait is voluntarily not derived in order to fully understand how rust's moves
// makes borrowing or explicitly copying (i.e., cloning) required for my code to compile :)
#[derive(Clone)]
pub struct UserChoiceState {
    battlelog_msg: String,
    // inv: ordered_choice_labels and ordered_choice_events are of the same len()
    // inv: ordered_choice_labels and ordered_choice_events match by index
    ordered_choice_labels: Vec<&'static str>,
    ordered_choice_events: Vec<UserChoiceEvent>,
    battlefield: Battlefield,
    state_enum: UserChoiceStateEnum,
}

impl UserChoiceState {

    pub(in crate::console) fn start(battlefield: Battlefield) -> Self {
        let field_player_monster_ids = battlefield
            .get_field_player_monsters()
            .iter()
            .map(|x| battlefield.get_player_monster_id(x).unwrap())
            .collect::<Vec<Uuid>>();
        let field_oppont_monster_ids = battlefield
            .get_field_oppont_monsters()
            .iter()
            .map(|x| battlefield.get_oppont_monster_id(x).unwrap())
            .collect::<Vec<Uuid>>();
        let first_monster = battlefield.get_field_player_monsters()[0];
        let id = battlefield.get_player_monster_id(first_monster).unwrap();
        // this creation of a default state avoids duplicating the code of the entry point state
        let _self = Self {
            // default parameters
            battlelog_msg: Default::default(),
            ordered_choice_labels: Default::default(),
            ordered_choice_events: Default::default(),
            state_enum: UserChoiceStateEnum::EndOfBattle,
            // useful parameters
            battlefield: battlefield,
        };
        match (field_player_monster_ids.len(), field_oppont_monster_ids.len()) {
            (0, _) => _self.get_state_end_of_battle(false),
            (_, 0) => _self.get_state_end_of_battle(true),
            _ => _self.get_state_entry_point(id),
        }
    }

    pub(in crate::console) fn battlefield(&self) -> &Battlefield { &self.battlefield }

    pub(in crate::console) fn battlelog_msg(&self) -> &String { &self.battlelog_msg }

    pub(in crate::console) fn ordered_choice_labels(&self) -> &Vec<&'static str> { 
        // Note: version without cloning, but requires the caller to hold a mutable reference of self.
        // This is afaik one of the two ways to avoid a copy, along with extensive boilerplate
        // (putting the vecs in the state_enum and exposing the state_enum when do_transition is called).
        // Note that in my opinion this has heavy strucural impacts for a minor performance improvement,
        // in most cases the caller should have to decide whether to clone the list or not, and most
        // of the time, they will iter/map/etc. on the returned collection anyway.

        // let mut result = Vec::new();
        // std::mem::swap(&mut result, &mut self.ordered_list_labels);
        // result

        &self.ordered_choice_labels
    }

    pub(in crate::console) fn ordered_choice_events(&self) -> &Vec<UserChoiceEvent> { 
        &self.ordered_choice_events 
    }

    pub(in crate::console) fn do_transition(self, event: UserChoiceEvent) -> Self {
        type State = UserChoiceStateEnum;
        type Event = UserChoiceEvent;
        let field_player_monster_ids = self.battlefield
            .get_field_player_monsters()
            .iter()
            .map(|x| self.battlefield.get_player_monster_id(x).unwrap())
            .collect::<Vec<Uuid>>();
        let field_oppont_monster_ids = self.battlefield
            .get_field_oppont_monsters()
            .iter()
            .map(|x| self.battlefield.get_oppont_monster_id(x).unwrap())
            .collect::<Vec<Uuid>>();
        let mut _self = self;
        let state = match (&_self.state_enum.clone(), event) {
            // end of battle
            _ if field_player_monster_ids.len() == 0 => {
                _self.get_state_end_of_battle(false)
            },
            _ if field_oppont_monster_ids.len() == 0 => {
                _self.get_state_end_of_battle(true)
            },
            (State::Done { .. }, Event::GoBack) => {
                _self.get_state_entry_point(field_player_monster_ids[0])
            },
            // State::EntryPointMonster
            (State::EntryPoint { id, .. }, Event::GoTeam) => {
                let eligible_incs = orchestrator::get_eligible_swap_incoming_monsters(*id);
                _self.get_state_team(*id, eligible_incs)
            },
            (State::EntryPoint { id, .. }, Event::GoMoves) => {
                _self.get_state_moves(*id)
            },
            (State::EntryPoint { id, .. }, Event::GoBack) if *id != field_player_monster_ids[0] => {
                let cur_position = field_player_monster_ids
                    .iter()
                    .position(|&x| x == *id)
                    .unwrap();
                let new_id = field_player_monster_ids[cur_position - 1];
                orchestrator::unregister_monster_action(new_id);
                _self.get_state_entry_point(new_id)
            },
            // State::Moves
            (State::Moves { id, .. }, Event::GoMove { src, mov }) => {
                let eligible_tars = orchestrator::get_eligible_move_target_monsters(src, mov);
                _self.get_state_targets(*id, eligible_tars, mov)
            },
            (State::Moves { id, .. }, Event::GoBack) => {
                _self.get_state_entry_point(*id)
            },
            // State::Targets
            (State::Targets { .. }, Event::DoCast { src, mov, tar, nxt }) => {
                let move_action = MoveAction::new(mov, src, tar);
                orchestrator::register_monster_move(move_action);
                if let Some(nxt) = nxt { 
                    _self.get_state_entry_point(nxt)
                } else { 
                    let last_turn_log = orchestrator::lock_action_registrations();
                    _self.get_state_done(last_turn_log)
                }
            },
            (State::Targets { id, .. }, Event::GoBack) => {
                _self.get_state_moves(*id)
            },
            // State::Team
            (State::Team { id, .. }, Event::GoTeamMonster { tar }) => {
                _self.get_state_team_monster_choice(*id, tar)
            },
            (State::Team { id, .. }, Event::GoBack) => {
                _self.get_state_entry_point(*id)
            },
            // State::TeamMonsterChoice
            (State::TeamMonsterChoice { .. }, Event::DoSwap { inc, out, nxt }) => {
                let swap_action = SwapAction::new(inc, out);
                orchestrator::register_monster_swap(swap_action);
                if let Some(nxt) = nxt {
                    _self.get_state_entry_point(nxt)
                } else {
                    let last_turn_log = orchestrator::lock_action_registrations();
                    _self.get_state_done(last_turn_log)
                }
            },
            (State::TeamMonsterChoice { id, .. }, Event::GoBack) => {
                let eligible_incs = orchestrator::get_eligible_swap_incoming_monsters(*id);
                _self.get_state_team(*id, eligible_incs)
            },
            _ => _self,
        };
        state
    }
    
    pub(in crate::console) fn is_done(&self) -> bool { matches!(self.state_enum, UserChoiceStateEnum::Done{..}) }

    pub(in crate::console) fn try_get_last_turn_log(&self) -> Option<&LastTurnLog> { 
        match &self.state_enum {
            UserChoiceStateEnum::Done { last_turn_log } => Some(&last_turn_log),
            _ => None,
        }
    }
}

// Copy trait is voluntarily not derived in order to fully understand how rust's moves
// makes borrowing or explicitly copying (i.e., cloning) required for my code to compile :)
#[derive(Clone)]
pub(in crate::console) enum UserChoiceEvent {
    GoBack,
    GoTeam,
    GoMoves,
    GoTeamMonster { 
        tar: MonsterId
    },
    GoMove { 
        src: MonsterId, 
        mov: MoveIdx 
    },
    DoSwap {
        out: MonsterId,
        inc: MonsterId,
        nxt: Option<MonsterId>,
    },
    DoCast { 
        src: MonsterId,
        mov: MoveIdx,
        tar: MonsterId,
        nxt: Option<MonsterId>,
    },
}

// private

// Copy trait is voluntarily not derived in order to fully understand how rust's moves
// makes borrowing or explicitly copying (i.e., cloning) required for my code to compile :)
#[derive(Clone)]
enum UserChoiceStateEnum {
    // What should Monster #x do?
    EntryPoint {
        id: MonsterId,
    },
    // Select a move..
    Moves {
        id: MonsterId,
    },
    // Select a target..
    Targets {
        id: MonsterId, 
    },
    // Choose a monster..
    Team {
        id: MonsterId,
    },
    // What to do with this Monster?
    TeamMonsterChoice {
        id: MonsterId, 
    },
    Done {
        last_turn_log: LastTurnLog,
    },
    EndOfBattle,
}

impl UserChoiceState {

    fn get_state_entry_point(self, id: MonsterId) -> Self {
        let ordered_list_labels = vec![
            "Moves",
            "Team",
        ];
        let ordered_next_events = vec![
            UserChoiceEvent::GoMoves,
            UserChoiceEvent::GoTeam,
        ];
        UserChoiceState {
            battlelog_msg: format!("What should {} do?", self.battlefield.get_monster(id).unwrap().get_species().get_name()),
            ordered_choice_events: ordered_next_events,
            ordered_choice_labels: ordered_list_labels,
            battlefield: self.battlefield,
            state_enum: UserChoiceStateEnum::EntryPoint { id },
        }
    }

    fn get_state_done(self, last_turn_log: LastTurnLog) -> Self {
        UserChoiceState {
            battlelog_msg: String::new(),
            battlefield: last_turn_log.get_final_battlefield().clone(),
            ordered_choice_events: vec![UserChoiceEvent::GoBack],
            ordered_choice_labels: vec!["Done :)"],
            state_enum: UserChoiceStateEnum::Done { last_turn_log },
        }
    }

    fn get_state_end_of_battle(self, has_player_won: bool) -> Self {
        let ordered_next_events = vec![UserChoiceEvent::GoBack];
        let ordered_list_labels = vec!["-"];
        let waiting_message = if has_player_won { 
            String::from("You have won :)") 
        } else { 
            String::from("You have lost :(") 
        };
        UserChoiceState {
            battlelog_msg: waiting_message,
            battlefield: self.battlefield,
            ordered_choice_events: ordered_next_events,
            ordered_choice_labels: ordered_list_labels,
            state_enum: UserChoiceStateEnum::EndOfBattle,
        }
    }

    fn get_state_team_monster_choice(self, id: MonsterId, tar: MonsterId) -> Self {
        let ordered_next_events = vec![
            UserChoiceEvent::DoSwap { out: id, inc: tar, nxt: get_nxt(&self.battlefield, id) },
        ];
        let ordered_list_labels = vec!["SWAP"];
        UserChoiceState {
            battlelog_msg: format!("What to do with {}?", self.battlefield.get_monster(tar).unwrap().get_species().get_name()),
            battlefield: self.battlefield,
            ordered_choice_events: ordered_next_events,
            ordered_choice_labels: ordered_list_labels,
            state_enum: UserChoiceStateEnum::TeamMonsterChoice { id },
        }
    }

    fn get_state_team(self, id: MonsterId, eligible_incs: Vec<MonsterId>) -> Self {
        let ordered_list_labels = eligible_incs
            .iter()
            .map(|&tar_id| self.battlefield.get_monster(tar_id).unwrap().get_species().get_name())
            .collect();
        let ordered_next_events = eligible_incs
            .iter()
            .map(|&tar| UserChoiceEvent::GoTeamMonster { tar })
            .collect();
        UserChoiceState {
            battlelog_msg: String::from("Choose a monster"),
            battlefield: self.battlefield,
            ordered_choice_events: ordered_next_events,
            ordered_choice_labels: ordered_list_labels,
            state_enum: UserChoiceStateEnum::Team { id },
        }
    }

    fn get_state_targets(self, id: MonsterId, eligible_tars: Vec<MonsterId>, mov: MoveIdx) -> Self {
        let nxt = get_nxt(&self.battlefield, id);
        let ordered_list_labels = eligible_tars
            .iter()
            .map(|&tar_id| self.battlefield.get_monster(tar_id).unwrap().get_species().get_name())
            .collect();
        let ordered_next_events = eligible_tars
            .iter()
            .map(|&tar| UserChoiceEvent::DoCast { src: id, mov, tar, nxt })
            .collect();
        UserChoiceState {
            battlelog_msg: String::from("Select a target"),
            battlefield: self.battlefield,
            ordered_choice_labels: ordered_list_labels,
            ordered_choice_events: ordered_next_events,
            state_enum: UserChoiceStateEnum::Targets { id },
        }
}

    fn get_state_moves(self, id: MonsterId) -> Self {
        let ordered_list_labels = self.battlefield
            .get_monster(id)
            .unwrap()
            .get_species()
            .get_moves()
            .iter()
            .map(|mov| mov.get_name())
            .collect();
        let ordered_next_events = self.battlefield
            .get_player_monster(id)
            .unwrap()
            .get_species()
            .get_moves()
            .iter()
            .enumerate()
            .map(|(i, _)| UserChoiceEvent::GoMove { src: id, mov: i as u8 })
            .collect();
        UserChoiceState {
            battlelog_msg: String::from("Select a move"),
            battlefield: self.battlefield,
            ordered_choice_labels: ordered_list_labels,
            ordered_choice_events: ordered_next_events,
            state_enum: UserChoiceStateEnum::Moves { id }
        }
    }
}

fn get_nxt(bf: &Battlefield, id: MonsterId) -> Option<MonsterId> {
    let field_player_monster_ids = bf
        .get_field_player_monsters()
        .iter()
        .map(|x| bf.get_player_monster_id(x).unwrap())
        .collect::<Vec<Uuid>>();
    if let Some(position) = field_player_monster_ids.iter().position(|&x| x == id) {
        if position + 1 < field_player_monster_ids.len() {
            Some(field_player_monster_ids[position + 1])
        } else {
            None
        }
    } else {
        None
    }
}
