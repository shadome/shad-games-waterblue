use crate::{model::*, console::dependencies::*};

type State = EndOfTurnStateEnum;
type Event = EndOfTurnEvent;

// public

#[derive(Clone)]
pub struct EndOfTurnState<TAIDependency, TBackEndDependency> where 
    TAIDependency: AIDependencyTrait,
    TBackEndDependency: BackEndDependencyTrait,
{
    battlelog_msg: String,
    // inv: ordered_choice_labels and ordered_choice_events are of the same len()
    // inv: ordered_choice_labels and ordered_choice_events match by index
    ordered_choice_labels: Vec<&'static str>,
    ordered_choice_events: Vec<EndOfTurnEvent>,

    last_turn_log: LastTurnLog,
    // index of the currently processed action in last_turn_log
    current_action_idx: u8,
    // index of the currently processed player replacement in last_turn_log
    current_replacement_idx: u8,

    current_battlefield: Battlefield,

    state_enum: EndOfTurnStateEnum,

    ai_dependency: TAIDependency,
    backend_dependency: TBackEndDependency,
}

impl<TAIDependency, TBackEndDependency> EndOfTurnState<TAIDependency, TBackEndDependency> where 
    TAIDependency: AIDependencyTrait,
    TBackEndDependency: BackEndDependencyTrait,
{

    // field getters

    pub(in crate::console) fn battlefield(&self) -> &Battlefield { &self.current_battlefield }

    pub(in crate::console) fn battlelog_msg(&self) -> &String { &self.battlelog_msg }

    pub(in crate::console) fn ordered_choice_labels(&self) -> &Vec<&'static str> { &self.ordered_choice_labels }

    pub(in crate::console) fn ordered_choice_events(&self) -> &Vec<Event> { &self.ordered_choice_events }

    #[cfg(test)]
    fn ai_dependency(&self) -> &TAIDependency { &self.ai_dependency }

    #[cfg(test)]
    fn backend_dependency(&self) -> &TBackEndDependency { &self.backend_dependency }

    #[cfg(test)]
    fn state_enum(&self) -> &EndOfTurnStateEnum { &self.state_enum }
    // functions

    pub(in crate::console) fn is_done(&self) -> bool { 
        match self.state_enum { 
            EndOfTurnStateEnum::Done => true, 
            _ => false,
        }
    }

    pub(in crate::console) fn start(
        last_turn_log: LastTurnLog,
        ai_dependency: TAIDependency,
        backend_dependency: TBackEndDependency,
    ) -> Self {
        let current_battlefield = last_turn_log.get_initial_battlefield().clone();
        let default = Self {
            ai_dependency,
            backend_dependency,
            current_action_idx: 0,
            current_replacement_idx: 0,
            last_turn_log,
            current_battlefield,
            state_enum: EndOfTurnStateEnum::PopNextActionLog, // default value so that do_transition(Event::GoNext) actually starts the FSM
            battlelog_msg: Default::default(),
            ordered_choice_labels: Default::default(),
            ordered_choice_events: Default::default(),
        };
        default.do_transition(EndOfTurnEvent::GoNext)
    }

    pub(in crate::console) fn do_transition(self, event: EndOfTurnEvent) -> Self {
        match (&self.state_enum, event) {
            // TR-01
            (State::PopNextActionLog, Event::GoNext) => self
                .try_unfold_swap_log()
                .try_unfold_move_log()
                .try_unfold_replacement_log()
                .try_render_updated_battlefield()
                .get_state_pop_next_action()
                .try_get_state_pop_next_required_replacement_from_pop_next_action_log()
                .try_get_state_done(),
            // TR-02
            (State::PopNextRequiredReplacement, Event::GoNext) => self
                .get_state_unfold_required_replacement(),
            // TR-41
            (State::UnfoldRequiredReplacement, Event::ReplacementChoice { incoming_id }) => self
                .register_replacement(incoming_id)
                .get_state_pop_next_required_replacement()
                // try_get_state_lock_replacements goes back to `start` and auto-triggers TR-01
                .try_get_state_lock_replacements(),
            // TR-42
            (State::UnfoldRequiredReplacement, Event::GoBack) => self
                .try_cancel_and_restore_previous_replacement()
                .get_state_unfold_required_replacement(),
            _ => self,
        }
    }

}

#[derive(Clone, Debug)]
pub(in crate::console) enum EndOfTurnEvent {
    GoNext,
    GoBack,
    ReplacementChoice {
        incoming_id: MonsterId,
    },
}

// private

#[derive(Clone, Debug)]
enum EndOfTurnStateEnum {
    PopNextActionLog,
    PopNextRequiredReplacement,
    UnfoldRequiredReplacement,
    Done,
}

impl<TAIDependency, TBackEndDependency> EndOfTurnState<TAIDependency, TBackEndDependency> where 
    TAIDependency: AIDependencyTrait,
    TBackEndDependency: BackEndDependencyTrait,
{

    fn get_state_pop_next_required_replacement(self) -> Self {
        let mut _self = self;
        _self.current_replacement_idx += 1;
        _self.state_enum = EndOfTurnStateEnum::PopNextRequiredReplacement;
        _self
    }

    fn get_state_pop_next_action(self) -> Self {
        let mut _self = self;
        _self.current_action_idx += 1;
        _self.state_enum = EndOfTurnStateEnum::PopNextActionLog;
        _self
    }

    fn try_get_state_pop_next_required_replacement_from_pop_next_action_log(self) -> Self {
        let has_next_action = self.current_action_idx < self.last_turn_log.get_actions().len() as u8 ;
        let has_next_req_replacement = self.current_replacement_idx < self.last_turn_log.get_required_replacements_player().len() as u8;
        if !has_next_action && has_next_req_replacement {
            let mut _self = self;
            _self.state_enum = State::PopNextRequiredReplacement;
            return _self;
        }
        self
    }
    
    fn try_get_state_done(self) -> Self {
        let has_next_action = self.current_action_idx < self.last_turn_log.get_actions().len() as u8;
        let has_next_req_replacement = self.current_replacement_idx < self.last_turn_log.get_required_replacements_player().len() as u8;
        if !has_next_action && !has_next_req_replacement {
            let mut _self = self;
            _self.state_enum = State::Done;
            return _self;
        }
        self
    }

    fn try_cancel_and_restore_previous_replacement(self) -> Self {
        if self.current_replacement_idx > 0 {
            let mut _self = self;
            let current_replacement_idx = _self.current_replacement_idx - 1;
            let outgoing_id = _self.last_turn_log.get_required_replacements_player()[current_replacement_idx as usize];
            _self.backend_dependency.unregister_replacement(outgoing_id);
            _self.current_replacement_idx = current_replacement_idx;
            return _self;
        }
        self
    }

    fn try_get_state_lock_replacements(self) -> Self {
        let has_next_req_replacement = self.current_replacement_idx < self.last_turn_log.get_required_replacements_player().len() as u8;
        if !has_next_req_replacement {
            let mut _self = self;
            let last_turn_log = _self.backend_dependency.lock_replacement_registrations();
            return Self::start(last_turn_log, _self.ai_dependency, _self.backend_dependency);
        }
        self
    }

    fn get_state_unfold_required_replacement(self) -> Self {
        let mut _self = self;
        let bf = _self.last_turn_log.get_initial_battlefield();
        let outgoing_id = _self.last_turn_log.get_required_replacements_player()[_self.current_replacement_idx as usize];
        let outgoing = bf.get_monster(outgoing_id).unwrap();
        let outgoing_name = outgoing.get_species().get_name();
        let eligible_incs = _self.backend_dependency.get_eligible_swap_incoming_monsters(outgoing_id);
        let ordered_list_labels = eligible_incs
            .iter()
            .map(|&incoming_id| bf.get_monster(incoming_id).unwrap().get_species().get_name())
            .collect();
        let ordered_next_events = eligible_incs
            .iter()
            .map(|&incoming_id| Event::ReplacementChoice { incoming_id })
            .collect();
        _self.battlelog_msg = format!("Choose a replacement for {}.", outgoing_name);
        _self.ordered_choice_labels = ordered_list_labels;
        _self.ordered_choice_events = ordered_next_events;
        _self.state_enum = State::UnfoldRequiredReplacement;
        _self
    }

    fn register_replacement(self, incoming_id: MonsterId) -> Self {
        let mut _self = self;
        let outgoing_id = _self.last_turn_log.get_required_replacements_player()[_self.current_replacement_idx as usize];
        let swap_action = SwapAction::new(incoming_id, outgoing_id);
        _self.backend_dependency.register_replacement(swap_action);
        _self
    }

    fn try_unfold_replacement_log(self) -> Self {
        let has_next_action = self.current_action_idx < self.last_turn_log.get_actions().len() as u8;
        if has_next_action {
            let bf = self.last_turn_log.get_initial_battlefield();
            let action_log = &self.last_turn_log.get_actions()[self.current_action_idx as usize];
            return match action_log { ActionLog::Replacement { swap_action, .. } => {
                let incoming_name = bf.get_monster(swap_action.get_incoming()).unwrap().get_species().get_name();
                let mut _self = self;
                _self.battlelog_msg = format!("Let's go, {}!", incoming_name);
                _self.ordered_choice_labels = vec!["▶"];
                _self.ordered_choice_events = vec![EndOfTurnEvent::GoNext];
                _self
            }, _ => self };
        }
        self
    }

    fn try_unfold_swap_log(self) -> Self {
        let has_next_action = self.current_action_idx < self.last_turn_log.get_actions().len() as u8;
        if has_next_action {
            let bf = self.last_turn_log.get_initial_battlefield();
            let action_log = &self.last_turn_log.get_actions()[self.current_action_idx as usize];
            return match action_log { ActionLog::Swap { action, .. } => {
                let outgoing_name = bf.get_monster(action.get_outgoing()).unwrap().get_species().get_name();
                let incoming_name = bf.get_monster(action.get_incoming()).unwrap().get_species().get_name();
                let mut _self = self;
                _self.battlelog_msg = format!("Come back, {}. Let's go, {}!", outgoing_name, incoming_name);
                _self.ordered_choice_labels = vec!["▶"];
                _self.ordered_choice_events = vec![EndOfTurnEvent::GoNext];
                _self
            }, _ => self };
        }
        self
    }

    fn try_unfold_move_log(self) -> Self {
        let has_next_action = self.current_action_idx < self.last_turn_log.get_actions().len() as u8;
        if has_next_action {
            let bf = self.last_turn_log.get_initial_battlefield();
            let action_log = &self.last_turn_log.get_actions()[self.current_action_idx as usize];
            return match action_log { ActionLog::Move { action, mitigation, .. } => {
                let caster = bf.get_monster(action.get_caster_id()).unwrap();
                let caster_name = caster.get_species().get_name();
                let move_name = caster.get_species().get_moves()[action.get_move_offset() as usize].get_name();
                let target_name = bf.get_monster(action.get_target_id()).unwrap().get_species().get_name();
                let has_knocked_out = mitigation.is_lethal();
                let battlelog_msg = match mitigation.get_mitigation() {
                    Mitigation::Damage(dmg) => format!("{} uses {} on {} for {} damage.", caster_name, move_name, target_name, dmg),
                    _ => format!("error, Mitigation is not Damage"),
                };
                let battlelog_msg = if has_knocked_out {
                    format!("{} {} is knocked out!", battlelog_msg, target_name)
                } else { 
                    battlelog_msg 
                };
                let mut _self = self;
                _self.battlelog_msg = battlelog_msg;
                _self.ordered_choice_labels = vec!["▶"];
                _self.ordered_choice_events = vec![EndOfTurnEvent::GoNext];
                _self
            }, _ => self };
        }
        self
    }

    fn try_render_updated_battlefield(self) -> Self {
        let has_actions = !self.last_turn_log.get_actions().is_empty();
        if has_actions {
            let current_battlefield = self.last_turn_log
                .get_actions()[self.current_action_idx as usize]
                .get_updated_battlefield()
                .clone();
            let mut _self = self;
            _self.current_battlefield = current_battlefield;
            return _self;
        }
        self
    }

}

#[cfg(test)]
mod end_of_turn_test {

    use crate::*;
    use super::*;
    use console::console_test_utils::*;
 
    #[test]
    fn test_unfold_swap_log_double_replacement_required() {
        /* setup */
        let level = Level::new(50).unwrap();
        let monster = Monster::new(Species::N001_BULBASAUR, level);
        let player_monsters = vec![monster.clone(),monster.clone(),monster.clone(),monster.clone()];
        let oppont_monsters = vec![monster.clone(),monster.clone(),monster.clone(),monster.clone()];
        let bf = Battlefield::new(player_monsters, oppont_monsters).unwrap();
        let monster_field_id_1 = bf.get_monster_id(bf.get_field_player_monsters()[0]).unwrap();
        let monster_field_id_2 = bf.get_monster_id(bf.get_field_player_monsters()[1]).unwrap();
        let monster_party_id_1 = bf.get_monster_id(bf.get_party_player_monsters()[0]).unwrap();
        let monster_party_id_2 = bf.get_monster_id(bf.get_party_player_monsters()[1]).unwrap();
        let last_turn_log = LastTurnLog::new(bf.clone(), Vec::new(), vec![monster_field_id_1, monster_field_id_2], Vec::new());
        let dummy_ltl = last_turn_log.clone(); // note: this value will never be read
        let ai = TestAI::new(
        /* get_action */ |_, _, _, _| panic!("unexpected call to get_action"), 
        /* get_replacement */ |_,_,_| panic!("unexpected call to get_replacement"),
        );
        let backend = TestBackEndApi::new(
        /* start_battle */ |_| panic!("unexpected call to start_battle"),
        /* register_monster_move */ |_| panic!("unexpected call to register_monster_move"),
        /* register_monster_swap */ |_| panic!("unexpected call to register_monster_swap"),
        /* register_replacement */ |_| { /* expected to be called - no return value */ },
        /* lock_action_reg */ || panic!("unexpected call to lock_action_reg"),
        /* lock_repl_reg */ || { /* note: this value will never be read */ dummy_ltl.clone() },
        /* unregister_monster_action */ |_| panic!("unexpected call to unregister_monster_action"),
        /* unregister_replacement */ |_| panic!("unexpected call to unregister_replacement"),
        /* get_eligible_swap_incoming_monsters */ |_| vec![monster_party_id_1, monster_party_id_2],
        /* get_eligible_move_target_monsters */ |_,_| panic!("unexpected call to get_eligible_move_target_monsters"),
        );
        /* test */
        // Start => Pop next action log => Pop next required replacement
        let result_bf = EndOfTurnState::start(last_turn_log, ai, backend);
        let pop_next_req_repl_1 = result_bf.state_enum().clone();
        // TR-02
        let result_bf = result_bf.do_transition(EndOfTurnEvent::GoNext);
        let register_replacement_1 = result_bf.state_enum().clone();
        // TR-41
        let result_bf = result_bf.do_transition(EndOfTurnEvent::ReplacementChoice { incoming_id: monster_party_id_1 });
        let pop_next_req_repl_2 = result_bf.state_enum().clone();
        // TR-02
        let result_bf = result_bf.do_transition(EndOfTurnEvent::GoNext);
        let register_replacement_2 = result_bf.state_enum().clone();
        // TR-41
        let result_bf = result_bf.do_transition(EndOfTurnEvent::ReplacementChoice { incoming_id: monster_party_id_2 });
        let pop_next_req_repl_3 = result_bf.state_enum().clone();
        // TODO end the FSM in this test
        /* assert */
        let updated_backend_dep = result_bf.backend_dependency();
        assert_eq!(updated_backend_dep.get_eligible_swap_incoming_monsters_cnt(), 2);
        assert_eq!(updated_backend_dep.register_replacement_cnt(), 2);
        assert_eq!(updated_backend_dep.lock_repl_reg_cnt(), 1);

        assert!(matches!(pop_next_req_repl_1, EndOfTurnStateEnum::PopNextRequiredReplacement));
        assert!(matches!(pop_next_req_repl_2, EndOfTurnStateEnum::PopNextRequiredReplacement));
        assert!(matches!(pop_next_req_repl_3, EndOfTurnStateEnum::PopNextRequiredReplacement));
        assert!(matches!(register_replacement_1, EndOfTurnStateEnum::UnfoldRequiredReplacement));
        assert!(matches!(register_replacement_2, EndOfTurnStateEnum::UnfoldRequiredReplacement));
    }
    

    // Note that in this case, it is the backend which decides which monster must be replaced,
    // i.e., the end_of_turn_fsm is provided with only one id to replace even though there were potentially two KOs
    // In other words, this function tests both one and two KOs for only one eligible replacement
    #[test]
    fn test_unfold_swap_log_single_replacement_required() {
        /* setup */
        let level = Level::new(50).unwrap();
        let monster = Monster::new(Species::N001_BULBASAUR, level);
        let player_monsters = vec![monster.clone(),monster.clone(),monster.clone()];
        let oppont_monsters = vec![monster.clone(),monster.clone(),monster.clone(),monster.clone()];
        let bf = Battlefield::new(player_monsters, oppont_monsters).unwrap();
        let monster_field_id_1 = bf.get_monster_id(bf.get_field_player_monsters()[0]).unwrap();
        let monster_party_id_1 = bf.get_monster_id(bf.get_party_player_monsters()[0]).unwrap();
        let last_turn_log_initial = LastTurnLog::new(bf.clone(), Vec::new(), vec![monster_field_id_1], Vec::new());
        let last_turn_log_round_2 = LastTurnLog::new(bf.clone(), Vec::new(), Vec::new(), Vec::new());
        let ai = TestAI::new(
        /* get_action */ |_, _, _, _| panic!("unexpected call to get_action"), 
        /* get_replacement */ |_,_,_| panic!("unexpected call to get_replacement"),
        );
        let backend = TestBackEndApi::new(
        /* start_battle */ |_| panic!("unexpected call to start_battle"),
        /* register_monster_move */ |_| panic!("unexpected call to register_monster_move"),
        /* register_monster_swap */ |_| panic!("unexpected call to register_monster_swap"),
        /* register_replacement */ |_| { /* expected to be called - no return value */ },
        /* lock_action_reg */ || panic!("unexpected call to lock_action_reg"),
        /* lock_repl_reg */ || { last_turn_log_round_2.clone() },
        /* unregister_monster_action */ |_| panic!("unexpected call to unregister_monster_action"),
        /* unregister_replacement */ |_| panic!("unexpected call to unregister_replacement"),
        /* get_eligible_swap_incoming_monsters */ |_| vec![monster_party_id_1],
        /* get_eligible_move_target_monsters */ |_,_| panic!("unexpected call to get_eligible_move_target_monsters"),
        );
        /* test */
        // Start => Pop next action log => Pop next required replacement
        let result_bf = EndOfTurnState::start(last_turn_log_initial, ai, backend);
        let pop_next_req_repl_1 = result_bf.state_enum().clone();
        // TR-02
        let result_bf = result_bf.do_transition(EndOfTurnEvent::GoNext);
        let register_replacement_1 = result_bf.state_enum().clone();
        // TR-41
        let result_bf = result_bf.do_transition(EndOfTurnEvent::ReplacementChoice { incoming_id: monster_party_id_1 });
        // At this point, the next transition should not be TR-02
        // because the FSM is not waiting on State::PopNextRequiredReplacement
        // => the FSM is done
        let done = result_bf.state_enum().clone();
        /* assert */
        let updated_backend_dep = result_bf.backend_dependency();
        assert_eq!(updated_backend_dep.get_eligible_swap_incoming_monsters_cnt(), 1);
        assert_eq!(updated_backend_dep.register_replacement_cnt(), 1);
        assert_eq!(updated_backend_dep.lock_repl_reg_cnt(), 1);

        assert!(matches!(pop_next_req_repl_1, EndOfTurnStateEnum::PopNextRequiredReplacement));
        assert!(matches!(register_replacement_1, EndOfTurnStateEnum::UnfoldRequiredReplacement));
        assert!(matches!(done, EndOfTurnStateEnum::Done));
        assert!(result_bf.is_done());
    }

    // todo test back to back to back replacements

    // todo test back to back replacement then actions

    // todo test actions and replacement then back to back to back the same

    // todo test actions without replacement then back to back to back the same

}


/*
TODO 221002
    * use the following fields in the FSM implementation:
        ai_dependency: TAIDependency,
        backend_dependency: TBackEndDependency,
    * all the backend callbacks should somehow appear on the FSM, i.e., both "get_...", else it is a whitebox test
        (use a mutable parameter inflated then passed on)

 */