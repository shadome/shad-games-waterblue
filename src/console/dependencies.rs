use crate::{model::*, orchestrator, robot};

// Note: dependencies need to be implemented through a struct
// in the console module, not their modules of reference (robot and orchestrator),
// think of the way a real crate or client/server segregation would be implemented.

pub trait AIDependencyTrait {
    
    fn get_action(
        &self,
        bf: &Battlefield, 
        req_caster: &Monster, 
        eligible_incomings: &Vec<MonsterId>,
        eligible_move_targets: &Vec<MonsterId>,
    ) -> Option<Action>;

    fn get_replacement(
        &self,
        bf: &Battlefield,
        req_replacement: &MonsterId,
        eligible_replacements: &Vec<MonsterId>,
    ) -> SwapAction;

}

pub(in crate::console) struct RobotAI {}

impl RobotAI {
    pub(in crate::console) fn new() -> Self { Self {} }
}

impl AIDependencyTrait for RobotAI {
    
    fn get_action(
        &self,
        bf: &Battlefield, 
        req_caster: &Monster, 
        eligible_incomings: &Vec<MonsterId>,
        eligible_move_targets: &Vec<MonsterId>,
    ) -> Option<Action> {
        robot::get_action(bf, req_caster, eligible_incomings, eligible_move_targets)
    }

    fn get_replacement(
        &self,
        bf: &Battlefield,
        req_replacement: &MonsterId,
        eligible_replacements: &Vec<MonsterId>,
    ) -> SwapAction {
        robot::get_replacement(bf, req_replacement, eligible_replacements)
    }

}

// TODO: this trait is only asking for mutable functions because
// I do not know how to implement it in a dedicated test struct
// while incrementing closure-captured variables such as `start_battle_call_count`
// which are required for test purposes.
// Those variables are for now fields in the test struct, which in turn requires mutable function calls...
// Ask the community about how to implement the behaviour I want using closures which can
// update their environment.
pub trait BackEndDependencyTrait {

    fn start_battle(&mut self, battlefield: Battlefield);

    fn unregister_monster_action(&mut self, monster_id: MonsterId);

    fn register_monster_move(&mut self, move_action: MoveAction);

    fn register_monster_swap(&mut self, swap_action: SwapAction);

    fn lock_action_registrations(&mut self) -> LastTurnLog;

    fn register_replacement(&mut self, swap_action: SwapAction);

    fn unregister_replacement(&mut self, outgoing_id: MonsterId);

    fn lock_replacement_registrations(&mut self) -> LastTurnLog;

    fn get_eligible_swap_incoming_monsters(&mut self, outgoing: MonsterId) -> Vec<MonsterId>;

    fn get_eligible_move_target_monsters(&mut self, caster: MonsterId, move_offset: u8) -> Vec<MonsterId>;

}

pub(in crate::console) struct BackEndApi {}

impl BackEndApi {
    pub(in crate::console) fn new() -> Self { Self {} }
}

impl BackEndDependencyTrait for BackEndApi {

    fn start_battle(&mut self, battlefield: Battlefield) {
        orchestrator::start_battle(battlefield);
    }

    fn unregister_monster_action(&mut self, monster_id: MonsterId) {
        orchestrator::unregister_monster_action(monster_id)
    }

    fn register_monster_move(&mut self, move_action: MoveAction) {
        orchestrator::register_monster_move(move_action)
    }

    fn register_monster_swap(&mut self, swap_action: SwapAction) {
        orchestrator::register_monster_swap(swap_action)
    }

    fn lock_action_registrations(&mut self) -> LastTurnLog {
        orchestrator::lock_action_registrations()
    }

    fn register_replacement(&mut self, swap_action: SwapAction) {
        orchestrator::register_replacement(swap_action)
    }

    fn unregister_replacement(&mut self, outgoing_id: MonsterId) {
        orchestrator::unregister_replacement(outgoing_id)
    }

    fn lock_replacement_registrations(&mut self) -> LastTurnLog {
        orchestrator::lock_replacement_registrations()
    }

    fn get_eligible_swap_incoming_monsters(&mut self, outgoing: MonsterId) -> Vec<MonsterId> {
        orchestrator::get_eligible_swap_incoming_monsters(outgoing)
    }

    fn get_eligible_move_target_monsters(&mut self, caster: MonsterId, move_offset: u8) -> Vec<MonsterId> {
        orchestrator::get_eligible_move_target_monsters(caster, move_offset)
    }

}