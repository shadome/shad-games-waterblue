use std::io::Stdout;

use tui::{Frame, backend::CrosstermBackend, layout::{Layout, Direction, Constraint}};

use super::utils::ActionListState;
use super::*;

mod field_layout;
mod action_layout;

pub(super) fn render(
    frame: &mut Frame<CrosstermBackend<Stdout>>,
    actions_state: &mut ActionListState,
    battlefield: &Battlefield,
    battlelog_msg: &String,
    ordered_choice_labels: &Vec<&'static str>,
) {
    /* Create divs in the main window */
    let size = frame.size();
    let divs = Layout::default()
        .direction(Direction::Vertical)
        .margin(0)
        .constraints([
            Constraint::Min(0), // scroll down
            Constraint::Length(field_layout::MIN_HEIGHT as u16),
            Constraint::Length(action_layout::MIN_HEIGHT as u16),
        ])
        .split(size);

    /* Associate content with divs and draw them for the current page */
    // div[0] is used for scrolling down
    field_layout::render(frame, divs[1], battlefield);
    action_layout::render(frame, actions_state, divs[2], battlelog_msg, ordered_choice_labels);
}
