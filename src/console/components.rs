// Components are, typically, drawable model counterparts.

use std::iter;

use tui::{style::{Style, Color}, text::{Span, Spans}};

use crate::model::*;

impl StatusCondition {
    pub(super) fn to_span<'a>(&self) -> Span<'a> {
        let fg_black_style = Style::default().fg(Color::Black);
        match self {
            StatusCondition::Burn => Span::styled(
                "⌜\u{2668} BRN⌝", 
                fg_black_style.bg(Color::LightRed)
            ),
            StatusCondition::Freeze => Span::styled(
                "⌜\u{2744} FZN⌝", 
                fg_black_style.bg(Color::LightBlue)
            ),
            StatusCondition::Infatuation => Span::styled(
                "⌜\u{2665} INF⌝", 
                fg_black_style.bg(Color::LightMagenta)
            ),
            StatusCondition::Paralysis => Span::styled(
                "⌜\u{26A1}PAR⌝",  
                fg_black_style.bg(Color::LightYellow)
            ),
            StatusCondition::Poison => Span::styled(
                "⌜\u{2620} PSN⌝", 
                fg_black_style.bg(Color::Magenta)
            ),
            StatusCondition::Toxic => Span::styled(
                "⌜\u{2620} TXC⌝", 
                fg_black_style.bg(Color::Magenta)
            ),
        }
    }
}

impl Monster {
    pub(super) fn to_spans<'a>(
        &self, 
        is_foe: bool,
    ) -> Vec<Spans<'a>> {
        let cur_hpt = self.get_cur_hpt();
        let max_hpt = self.get_max_hpt();
        let level = self.get_level().as_u8();
        let name = self.get_species().get_name();
        let status = self
            .get_status()
            .map(|status| status.to_span())
            .unwrap_or_else(|| Span::from(""))
        ;
        let health_pct = self.get_cur_hpt() as u16 * 100 / self.get_max_hpt() as u16;
        let health_perten = health_pct / 10;
        let healthbar_fg_char_span = Span::styled(
            "\u{25B0}", 
            Style::default().fg(Color::Green),
        );
        let healthbar_bg_char_span = Span::styled(
            "\u{25B0}", 
            Style::default().fg(Color::Black),
        );
        let healthbar = Vec::new()
            .into_iter()
            .chain(iter::repeat(healthbar_fg_char_span).take(health_perten as usize))
            .chain(iter::repeat(healthbar_bg_char_span).take(10 - health_perten as usize))
            .collect::<Vec<Span>>()
        ;
        let health_detail_span = match is_foe {
            true  => Span::from(""),
            false => Span::from(format!(" {cur_hpt}/{max_hpt}")),
        };
        return vec![
            Spans::from(format!(" {name} (Lvl {level})")),
            Spans::from(
                Vec::new()
                    .into_iter()
                    .chain(healthbar)
                    .chain([health_detail_span])
                    .chain([Span::raw(format!(" ({health_pct}%)"))])
                    .chain([Span::from(" "), status])
                    .collect::<Vec<Span>>()
            ),
        ];
    }
}