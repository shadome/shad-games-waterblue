use std::{io::Stdout};
use tui::{Frame, backend::CrosstermBackend, layout::{Rect, Constraint, Direction, Layout} };

use crate::model::Battlefield;

mod battlefield_layout;
mod teambar_layout;

// public

pub(super) const MIN_HEIGHT: u8 = BF_DIV_MIN_HEIGHT + TEAMS_DIV_MIN_HEIGHT;

pub(super) fn render(
    frame: &mut Frame<CrosstermBackend<Stdout>>, 
    area: Rect,
    battlefield: &Battlefield,
) {
    let bf_col = Layout::default()
        .direction(Direction::Vertical)
        .constraints([
            Constraint::Length(TEAMS_DIV_MIN_HEIGHT as u16),
            Constraint::Length(BF_DIV_MIN_HEIGHT as u16),
        ])
        .split(area)
    ;
    teambar_layout::render(frame, bf_col[0], battlefield);
    battlefield_layout::render(frame, bf_col[1], battlefield);
}

// private

const BF_DIV_MIN_HEIGHT: u8 = 9;
const TEAMS_DIV_MIN_HEIGHT: u8 = 3;
