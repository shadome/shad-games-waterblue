use std::io::Stdout;

use tui::{Frame, backend::CrosstermBackend, layout::Rect, widgets::{Block, BorderType, Borders, List, ListItem}, style::{Color, Style, Modifier}, text::Spans};

use crate::{console::{utils::ActionListState, *}};

// public

pub(super) fn render<'a>(
    frame: &mut Frame<CrosstermBackend<Stdout>>, 
    actions_state: &mut ActionListState,
    area: Rect,
    ordered_choice_labels: &Vec<&'static str>,
) {
    let block = Block::default()
        .title(" Actions ")
        .borders(Borders::ALL)
        .border_type(BorderType::Double);
    let highlight_style = Style::default()
        .bg(Color::Green)
        .fg(Color::Black)
        .add_modifier(Modifier::BOLD);
    let actions_spans = ordered_choice_labels
        .iter()
        .map(|str| ListItem::new(Spans::from(*str)))
        .collect::<Vec<ListItem>>();
    actions_state.len = actions_spans.len();
    if let Some(len) = actions_state.list_state.selected() {
        if len >= actions_state.len {
            actions_state.list_state.select(Some(0));
        }
    }
    let actions_div = List
        ::new(actions_spans)
        .block(block)
        .highlight_style(highlight_style)
        // .highlight_symbol("\u{25B6}")
    ;
    frame.render_stateful_widget(actions_div, area, &mut actions_state.list_state);
}
