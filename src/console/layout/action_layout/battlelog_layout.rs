use std::io::Stdout;

use tui::{Frame, backend::CrosstermBackend, layout::Rect, widgets::{Block, BorderType, Borders, Paragraph}, text::Spans};

// public

pub(super) fn render<'a>(
    frame: &mut Frame<CrosstermBackend<Stdout>>, 
    area: Rect,
    battlelog_msg: &String,
) {
    let block = Block::default()
        .title(" Text ")
        .borders(Borders::ALL)
        .border_type(BorderType::Plain)
    ;
    let text_div = Paragraph
        ::new(vec![
            Spans::from(""),
            Spans::from(battlelog_msg.clone()),
        ])
        .block(block)
    ;
    frame.render_widget(text_div, area);
}
