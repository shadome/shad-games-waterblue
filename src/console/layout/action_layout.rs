use std::io::Stdout;

use tui::{Frame, backend::CrosstermBackend, layout::{Rect, Constraint, Direction, Layout}};

use crate::console::{utils::ActionListState, *};

mod actionlist_layout;
mod battlelog_layout;

// public

pub(super) const MIN_HEIGHT: u16 = 6;

pub(super) fn render(
    frame: &mut Frame<CrosstermBackend<Stdout>>, 
    actions_state: &mut ActionListState,
    area: Rect,
    battlelog_msg: &String,
    ordered_choice_labels: &Vec<&'static str>,
) {
    let bot_row = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([
            Constraint::Percentage(60),
            Constraint::Percentage(40),
        ])
        .split(area);
    battlelog_layout::render(frame, bot_row[0], battlelog_msg);
    actionlist_layout::render(frame, actions_state, bot_row[1], ordered_choice_labels);
}
