use std::{io::Stdout, iter};
use tui::{Frame, backend::CrosstermBackend, layout::{Rect, Constraint, Direction, Layout}, widgets::{Block, BorderType, Borders, Paragraph}, style::{Color, Style }, text::{Spans, Span}};

use crate::model::*;

// public

pub(super) fn render(
    frame: &mut Frame<CrosstermBackend<Stdout>>, 
    area: Rect,
    battlefield: &Battlefield,
) {
    let teams_row = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([
            Constraint::Percentage(50),
            Constraint::Percentage(50),
        ])
        .split(area);
    let play_team_div = get_team_div(
        PLAYER_TEAM_BLOCK_TITLE,
        count_monsters(battlefield.get_player_team(), false),
        count_monsters(battlefield.get_player_team(), true),
    );
    let oppo_team_div = get_team_div(
        OPPONT_TEAM_BLOCK_TITLE, 
        count_monsters(battlefield.get_oppont_team(), false), 
        count_monsters(battlefield.get_oppont_team(), true),
    );
    frame.render_widget(play_team_div, teams_row[0]);
    frame.render_widget(oppo_team_div, teams_row[1]);
    
}

// private

const PLAYER_TEAM_BLOCK_TITLE: &'static str = " Player team ";
const OPPONT_TEAM_BLOCK_TITLE: &'static str = " Opponent team ";

fn count_monsters(
    team: Vec<&Monster>,
    should_be_ko: bool,
) -> usize {
    team
        .iter()
        .filter(|_monster| { should_be_ko == _monster.is_ko() })
        .count()
}

fn get_team_div<'a>(
    title: &'a str,
    nb_monsters_ok: usize,
    nb_monsters_ko: usize,
) -> Paragraph<'a> {
    let team_size = 6;
    let ok_char_span = Span::styled(
        "\u{2B24}", 
        Style::default().fg(Color::Green),
    );
    let ko_char_span = Span::styled(
        "\u{2B24}", 
        Style::default().fg(Color::LightRed),
    );
    let na_char_span = Span::styled(
        "\u{2B24}", 
        Style::default().fg(Color::Black),
    );
    let block = Block::default()
        .title(title)
        .borders(Borders::ALL)
        .border_type(BorderType::Plain)
    ;
    let team_bar = Vec::new()
        .into_iter()
        .chain(iter::repeat(ok_char_span).take(nb_monsters_ok as usize))
        .chain(iter::repeat(ko_char_span).take(nb_monsters_ko as usize))
        .chain(iter::repeat(na_char_span).take(team_size - nb_monsters_ko - nb_monsters_ok))
        .collect::<Vec<Span>>()
    ;
    Paragraph
        ::new(Spans::from(team_bar))
        .block(block)
}