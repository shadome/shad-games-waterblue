use std::{io::Stdout};
use tui::{Frame, backend::CrosstermBackend, layout::{Rect, Constraint, Direction, Layout}, widgets::{Block, BorderType, Borders, Paragraph}, text::Spans};

use crate::model::*;

const PLAYER_BATTLEFIELD_BLOCK_TITLE: &'static str = " Player battlefield ";
const OPPONT_BATTLEFIELD_BLOCK_TITLE: &'static str = " Opponent battlefield ";

// public

pub(super) fn render(
    frame: &mut Frame<CrosstermBackend<Stdout>>, 
    area: Rect,
    battlefield: &Battlefield,
) {
    let bf_row = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([
            Constraint::Percentage(50),
            Constraint::Percentage(50),
        ])
        .split(area)
    ;
    let play_bf_div = get_bf_div(
        PLAYER_BATTLEFIELD_BLOCK_TITLE, 
        false,
        battlefield.get_field_player_monsters(),
    );
    let oppo_bf_div = get_bf_div(
        OPPONT_BATTLEFIELD_BLOCK_TITLE, 
        true,
        battlefield.get_field_oppont_monsters(),
    );
    frame.render_widget(play_bf_div, bf_row[0]);
    frame.render_widget(oppo_bf_div, bf_row[1]);
    

}

// private

fn get_bf_div<'a>(
    title: &'static str,
    is_foe: bool,
    monsters: Vec<&Monster>,
) -> Paragraph<'a> {
    let block = Block::default()
        .title(title)
        .borders(Borders::ALL)
        .border_type(BorderType::Plain)
    ;
    let mut result = vec![Spans::from("")];
    for monster in monsters {
        result.extend(monster.clone().to_spans(is_foe));
        result.extend([Spans::from("")]);
    }
    Paragraph::new(result).block(block)
}