use tui::widgets::ListState;

pub(super) struct ActionListState {
    pub(super) len: usize,
    pub(super) list_state: ListState,
}

impl Default for ActionListState {
    fn default() -> Self {
        Self { len: 0, list_state: Default::default() }
    }
}

impl ActionListState {
    
    pub(super) fn select_prev(&mut self) -> &mut Self {
        if self.len == 0 { 
            return self; 
        }
        if let Some(selected) = self.list_state.selected() {
            let prev_idx = match selected <= 0 {
                true => self.len - 1,
                false => selected - 1,
            };
            self.list_state.select(Some(prev_idx));
        }
        self
    }

    pub(super) fn select_next(&mut self) -> &mut Self {
        if self.len == 0 { 
            return self; 
        }
        if let Some(selected) = self.list_state.selected() {
            let next_idx = match selected >= self.len - 1 {
                true => 0,
                false => selected + 1,
            };
            self.list_state.select(Some(next_idx));
        }
        self
    }

}