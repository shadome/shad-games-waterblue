
#[cfg(test)]
mod test {

    pub struct TestAI<FnGetAction, FnGetRepl> where 
        FnGetAction: Fn(&Battlefield, &Monster, &Vec<MonsterId>, &Vec<MonsterId>) -> Option<Action>,
        FnGetRepl: Fn(&Battlefield, &MonsterId, &Vec<MonsterId>) -> SwapAction,
    {
        get_action: FnGetAction,
        get_replacement: FnGetRepl,
    }

    impl<FnGetAction, FnGetRepl> TestAI<FnGetAction, FnGetRepl> where 
        FnGetAction: Fn(&Battlefield, &Monster, &Vec<MonsterId>, &Vec<MonsterId>) -> Option<Action>,
        FnGetRepl: Fn(&Battlefield, &MonsterId, &Vec<MonsterId>) -> SwapAction,
    {
        pub fn new(get_action: FnGetAction, get_replacement: FnGetRepl) -> Self { 
            Self { get_action, get_replacement } 
        }
    }
    
    impl<FnGetAction, FnGetRepl> AIDependencyTrait for TestAI<FnGetAction, FnGetRepl> where 
        FnGetAction: Fn(&Battlefield, &Monster, &Vec<MonsterId>, &Vec<MonsterId>) -> Option<Action>,
        FnGetRepl: Fn(&Battlefield, &MonsterId, &Vec<MonsterId>) -> SwapAction,
    {
        
        fn get_action(&self, bf: &Battlefield, req_caster: &Monster, eligible_incomings: &Vec<MonsterId>, eligible_move_targets: &Vec<MonsterId>) -> Option<Action> {
            (self.get_action)(bf, req_caster, eligible_incomings, eligible_move_targets)
        }
    
        fn get_replacement(&self, bf: &Battlefield, req_replacement: &MonsterId, eligible_replacements: &Vec<MonsterId>) -> SwapAction {
            (self.get_replacement)(bf, req_replacement, eligible_replacements)
        }
    
    }
    
    pub struct TestBackEndApi<FnStart, FnLockActionReg, FnLockReplReg> where
        FnStart: Fn(Battlefield),
        FnLockActionReg: Fn() -> LastTurnLog,
        FnLockReplReg: Fn() -> LastTurnLog,
    {
        start_battle: FnStart,
        lock_action_reg: FnLockActionReg,
        lock_repl_reg: FnLockReplReg,
    }

    impl<FnStart, FnLockActionReg, FnLockReplReg> TestBackEndApi<FnStart, FnLockActionReg, FnLockReplReg> where
        FnStart: Fn(Battlefield),
        FnLockActionReg: Fn() -> LastTurnLog,
        FnLockReplReg: Fn() -> LastTurnLog,
    {
        pub fn new(start_battle: FnStart, lock_action_reg: FnLockActionReg, lock_repl_reg: FnLockReplReg) -> Self { 
            Self { start_battle, lock_action_reg, lock_repl_reg } 
        }
    }

    impl<FnStart, FnLockActionReg, FnLockReplReg> BackEndDependencyTrait for TestBackEndApi<FnStart, FnLockActionReg, FnLockReplReg> where 
        FnStart: Fn(Battlefield),
        FnLockActionReg: Fn() -> LastTurnLog,
        FnLockReplReg: Fn() -> LastTurnLog,
    {

        fn start_battle(&self, battlefield: Battlefield) { 
            (self.start_battle)(battlefield) 
        }

        fn unregister_monster_action(&self, monster_id: MonsterId) { }

        fn register_monster_move(&self, move_action: MoveAction) { }

        fn register_monster_swap(&self, swap_action: SwapAction) { }

        fn lock_action_registrations(&self) -> LastTurnLog { 
            (self.lock_action_reg)()
        }

        fn register_replacement(&self, swap_action: SwapAction) { }

        fn unregister_replacement(&self, outgoing_id: MonsterId) { }

        fn lock_replacement_registrations(&self) -> LastTurnLog { (self.lock_repl_reg)() }

        fn get_eligible_swap_incoming_monsters(&self, outgoing: MonsterId) -> Vec<MonsterId> {
            // orchestrator::get_eligible_swap_incoming_monsters(outgoing)
            // TODO
            Vec::new()
        }

        fn get_eligible_move_target_monsters(&self, caster: MonsterId, move_offset: u8) -> Vec<MonsterId> {
            // orchestrator::get_eligible_move_target_monsters(caster, move_offset)
            // TODO
            Vec::new()
        }

    }

    use crate::*;
    use super::*;
 
    #[test]
    fn test_unfold_swap_log_double_replacement_required() {
        // setup

        let level = Level::new(50).unwrap();
        let player_monsters = vec![
            Monster::new(Species::N001_BULBASAUR, level).set_cur_hpt(0),
            Monster::new(Species::N001_BULBASAUR, level).set_cur_hpt(0),
            Monster::new(Species::N001_BULBASAUR, level),
            Monster::new(Species::N001_BULBASAUR, level),
        ];
        let oppont_monsters = vec![
            Monster::new(Species::N007_SQUIRTLE, level),
            Monster::new(Species::N007_SQUIRTLE, level),
            Monster::new(Species::N007_SQUIRTLE, level),
            Monster::new(Species::N007_SQUIRTLE, level),
        ];
        let bf = Battlefield::new(player_monsters, oppont_monsters).unwrap();
        let initial_bf = bf.clone();
        let monster_field_id_1 = bf.get_monster_id(bf.get_field_player_monsters()[0]).unwrap();
        let monster_field_id_2 = bf.get_monster_id(bf.get_field_player_monsters()[1]).unwrap();
        let monster_party_id_1 = bf.get_monster_id(bf.get_field_player_monsters()[0]).unwrap();
        let monster_party_id_2 = bf.get_monster_id(bf.get_field_player_monsters()[1]).unwrap();
        let last_turn_log = LastTurnLog::new(bf.clone(), Vec::new(), vec![monster_field_id_1, monster_field_id_2], Vec::new());

        let backend_api = BackEndApi::new(); // TODO
        let robot_ai = RobotAI::new(); // TODO

        // let l1 = initial_bf[Team::Player].clone();
        // let l2 = &initial_bf[monster_field_id_1];
        // test
        orchestrator::start_battle(bf);
        let result_bf = EndOfTurnState::start(last_turn_log, robot_ai, backend_api)
            // Pop next action log => Pop next required replacement
            .do_transition(EndOfTurnEvent::GoNext)
            // TR-02
            .do_transition(EndOfTurnEvent::GoNext)
            // TR-41
            .do_transition(EndOfTurnEvent::ReplacementChoice { incoming_id: monster_party_id_1 })
            // TR-02
            .do_transition(EndOfTurnEvent::GoNext)
            // TR-41
            .do_transition(EndOfTurnEvent::ReplacementChoice { incoming_id: monster_party_id_2 })
            
        ;

        // assert
        // assert_eq!(initial_bf, bf);
    }
}
