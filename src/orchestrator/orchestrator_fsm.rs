use crate::{model::*, robot, battlecore};
use crate::model::HasCaster;
use crate::model::HasUpdatedBattlefield;

#[derive(Clone)]
pub(super) struct OrchestratorState {
    battlefield: Battlefield,
    state_enum: OrchestratorStateEnum,
    // Note: This feels concurrent to the principle of FSMs:
    // it is a value dependent on the previous state, not the current one
    last_turn_log: Option<LastTurnLog>,
}

impl OrchestratorState {
    pub(super) fn battlefield(&self) -> &Battlefield { &self.battlefield }
    pub(super) fn state_enum(&self) -> &OrchestratorStateEnum { &self.state_enum }
    pub(super) fn last_turn_log(&self) -> &Option<LastTurnLog> { &self.last_turn_log }

    pub(super) fn new(battlefield: Battlefield) -> Self {
        let state_enum = OrchestratorStateEnum::ActionRegistration {
            pending_actions: Vec::with_capacity(FIELD_MONSTERS_MAX_NB as usize * 2),
        };
        Self { battlefield, state_enum, last_turn_log: None}
    }

    fn new_with_log(log: LastTurnLog) -> Self {
        let state_enum = OrchestratorStateEnum::ActionRegistration {
            pending_actions: Vec::with_capacity(FIELD_MONSTERS_MAX_NB as usize * 2),
        };
        let battlefield = log.get_final_battlefield().clone();
        Self { battlefield, state_enum, last_turn_log: Some(log) }
    }

    pub(super) fn do_transition(self, event: OrchestratorEvent) -> Self {
        type Event = OrchestratorEvent;
        type State = OrchestratorStateEnum;
        let mut _self = self;
        _self.last_turn_log = None;
        let new_state = match(&_self.state_enum, event) {
            (State::ActionRegistration { pending_actions }, Event::RegisterSwapAction { swap_action }) => {

                fn is_monster_already_incoming(pending: &Vec<Action>, incoming: MonsterId) -> bool {
                    pending.iter().any(|action_iter| match action_iter {
                        Action::Swap(swap_action) => swap_action.get_incoming() == incoming,
                        _ => false,
                    })
                }

                let mut pending_actions = Self::remove_pending_actions_from_caster(pending_actions, swap_action.get_outgoing());
                let is_incoming_eligible = true
                    && !is_monster_already_incoming(&pending_actions, swap_action.get_incoming())
                    && !_self.battlefield.get_monster(swap_action.get_incoming()).unwrap().is_ko()
                ;
                let is_outgoing_eligible = !_self.battlefield.get_monster(swap_action.get_outgoing()).unwrap().is_ko();
                if is_incoming_eligible && is_outgoing_eligible {
                    // Note that the diagram's "remove existig actions for outgoing" is performed here because it is
                    // in this block that the updated pending_actions are assigned to "new_state"
                    pending_actions.push(Action::Swap(swap_action));
                    let state_enum = OrchestratorStateEnum::ActionRegistration { pending_actions };
                    Self { battlefield: _self.battlefield, state_enum, last_turn_log: None }
                } else {
                    _self
                }
            },
            (State::ActionRegistration { pending_actions }, Event::RegisterMoveAction { move_action }) => {
                let mut pending_actions = Self::remove_pending_actions_from_caster(pending_actions, move_action.get_caster_id());
                let can_caster_cast = !_self.battlefield.get_monster(move_action.get_caster_id()).unwrap().is_ko();
                let can_target_be_targeted = !_self.battlefield.get_monster(move_action.get_target_id()).unwrap().is_ko();
                if can_caster_cast && can_target_be_targeted {
                    // Note that the diagram's "remove existig actions for outgoing" is performed here because it is
                    // in this block that the updated pending_actions are assigned to "new_state"
                    pending_actions.push(Action::Move(move_action));
                    let state_enum = OrchestratorStateEnum::ActionRegistration { pending_actions };
                    Self { battlefield: _self.battlefield, state_enum, last_turn_log: None }
                } else {
                    _self
                }
            },
            (State::ActionRegistration { pending_actions }, Event::UnregisterAction { caster_id }) => {
                let pending_actions = Self::remove_pending_actions_from_caster(pending_actions, caster_id);
                let state_enum = OrchestratorStateEnum::ActionRegistration { pending_actions };
                Self { battlefield: _self.battlefield, state_enum, last_turn_log: None }
            },
            (State::ActionRegistration { pending_actions }, Event::LockActionRegistrations { .. }) => {
                // TODO this will move to the console
                let robot_actions = _self.battlefield
                    .get_field_oppont_monsters()
                    .iter()
                    // TODO this is a bug egg, the get_eligible_move_target_monsters return value
                    // does not take into account previous registrations (former get_action values)
                    // this code would not work if get_action sometimes returned a swap
                    .filter_map(|caster| robot::get_action(
                        &_self.battlefield,
                        caster,
                        &Vec::new(),
                        &super::get_eligible_move_target_monsters(_self.battlefield.get_monster_id(caster).unwrap(), 0 as u8)))
                    .collect::<Vec<Action>>();
                
                let pending_actions = Vec::new()
                    .into_iter()
                    .chain(robot_actions.clone())
                    .chain(pending_actions.clone())
                    .collect();
                let last_turn_log = battlecore::play_turn(_self.battlefield, &pending_actions);
                let battlefield = last_turn_log.get_actions().last().unwrap().get_updated_battlefield().clone();
                let req_replacements_player = last_turn_log.get_required_replacements_player();
                let req_replacements_oppont = last_turn_log.get_required_replacements_oppont();
                let needs_monster_replacement_for_oppont = !req_replacements_oppont.is_empty();
                let needs_monster_replacement_for_player = !req_replacements_player.is_empty();
                let needs_monster_replacement = needs_monster_replacement_for_player || needs_monster_replacement_for_oppont;
                let mut pending_replacements = req_replacements_player
                    .iter()
                    .map(|monster_id| (*monster_id, None))
                    .collect::<Vec<(MonsterId, Option<MonsterId>)>>();

                if !needs_monster_replacement {
                    // => next state is  action registration
                    // Note that shift_ko_field_monsters is required here because there could be ko monsters on field
                    // which could not be replaceable (no eligible incoming monsters)
                    let last_turn_log = last_turn_log.set_final_battlefield(battlefield.shift_ko_field_monsters());
                    Self::new_with_log(last_turn_log)
                } else {
                    // => there may be replacements needed either from the robot, the player, or both
                    let eligible_replacements = battlefield.get_party_oppont_monsters()
                        .into_iter()
                        .filter(|monster| !monster.is_ko())
                        .collect::<Vec<&Monster>>();
                    let mut robot_replacements = robot::get_replacements(&battlefield, &req_replacements_oppont, &eligible_replacements)
                        .iter()
                        .map(|swap_action| (swap_action.get_outgoing(), Some(swap_action.get_incoming())))
                        .collect();
                    pending_replacements.append(&mut robot_replacements);
                    
                    if !needs_monster_replacement_for_player {
                        // => there are only ko monsters on field for the robot,
                        // apply the robot replacements, 
                        // next state is action registration
                        let initial_replacements = pending_replacements
                            .iter()
                            .map(|&(outgoing, incoming_opt)| SwapAction::new(incoming_opt.unwrap(), outgoing))
                            .collect();
                        let replacement_logs = Self::update_battlefield_with_replacements(battlefield, &initial_replacements);
                        let last_turn_log = last_turn_log.add_action_logs(replacement_logs);
                        Self::new_with_log(last_turn_log)
                    } else {
                        // => there are at least one ko monster on field for the player
                        let state_enum = OrchestratorStateEnum::ReplacementRegistration { pending_replacements };
                        let last_turn_log = Some(last_turn_log);
                        Self { battlefield, state_enum, last_turn_log }
                    }
                }
            },
            (State::ReplacementRegistration { pending_replacements }, Event::RegisterReplacement { swap_action }) => {
                let replacement = swap_action.get_incoming();
                let knocked_out = swap_action.get_outgoing();
                let mut pending_replacements = Self::remove_pending_replacements_for_ko_monster(pending_replacements, knocked_out);
                let is_already_incoming = pending_replacements.iter().any(|&(_, incoming_opt)| Some(replacement) == incoming_opt);
                let is_incoming_eligible = true
                    && !is_already_incoming
                    && !_self.battlefield.get_monster(replacement).unwrap().is_ko()
                ;
                let is_outgoing_eligible = pending_replacements.iter().any(|&(ko, _)| ko == knocked_out);
                if is_incoming_eligible && is_outgoing_eligible {
                    // Note that the diagram's "remove existig actions for outgoing" is performed here because it is
                    // in this block that the updated pending_actions are assigned to "new_state"
                    pending_replacements.push((knocked_out, Some(replacement)));
                    let state_enum = OrchestratorStateEnum::ReplacementRegistration { pending_replacements };
                    Self { battlefield: _self.battlefield, state_enum, last_turn_log: None }
                } else {
                    _self
                }
            },
            (State::ReplacementRegistration { pending_replacements }, Event::UnregisterReplacement { knocked_out }) => {
                let pending_replacements = Self::remove_pending_replacements_for_ko_monster(pending_replacements, knocked_out);
                let state_enum = OrchestratorStateEnum::ReplacementRegistration { pending_replacements };
                Self { battlefield: _self.battlefield, state_enum, last_turn_log: None }
            },
            (State::ReplacementRegistration { pending_replacements }, Event::LockReplacementRegistrations { .. }) => {
                // Note: this code trusts the robot to correctly register all its required replacements,
                // hence only filling up the player's missing replacements with player monsters
                let mut swap_actions = pending_replacements
                    .iter()
                    .filter_map(|&(ko, repl_opt)| repl_opt.is_some().then(|| SwapAction::new(repl_opt.unwrap(), ko)))
                    .collect::<Vec<SwapAction>>();
                let missing_replacements = pending_replacements
                    .iter()
                    .filter_map(|&(ko, repl_opt)| repl_opt.is_none().then(|| ko))
                    .collect::<Vec<MonsterId>>();
                if !missing_replacements.is_empty() {
                    // Note that this code only checks the player for missing replacements
                    let already_coming_in = pending_replacements
                        .iter()
                        .filter_map(|&(_, repl_opt)| repl_opt)
                        .collect::<Vec<MonsterId>>();
                    let eligible_replacements = _self.battlefield.get_party_player_monsters()
                        .into_iter() // into_iter required because the source vec is already over references (&T), we want to copy the reference (&T -> &T), not reference the references (&&T)
                        // TODO 
                        // This should use the same business logic as
                        // orchestrator::get_eligible_swap_incoming_monsters
                        // does, which does not exist at this point,
                        // i.e., battlecore::can_swap_in (ko) + orchestration::can_swap_in (ok, kinda)
                        .filter(|&monster| true 
                            && !monster.is_ko() 
                            && !already_coming_in.contains(&_self.battlefield.get_monster_id(monster).unwrap()))
                        .collect();
                    let mut missing_swap_actions = robot::get_replacements(&_self.battlefield, &missing_replacements, &eligible_replacements);
                    swap_actions.append(&mut missing_swap_actions);
                }
                let replacement_logs = Self::update_battlefield_with_replacements(_self.battlefield.clone(), &swap_actions);
                let last_turn_log = LastTurnLog::new(
                    _self.battlefield.clone(), 
                    replacement_logs,
                    Vec::new(),
                    Vec::new());
                Self::new_with_log(last_turn_log)
            },
            _ => _self,
        };
        new_state
    }
    
    fn update_battlefield_with_replacements(battlefield: Battlefield, pending_replacements: &Vec<SwapAction>) -> Vec<ActionLog> {
        let mut replacements = Vec::new();
        let _ = pending_replacements
            .iter()
            .enumerate()
            .fold(
                /* init */ battlefield,
                |battlefield, (idx, repl)| {
                    let updated_battlefield = battlefield.swap_monsters(repl.get_outgoing(), repl.get_incoming());
                    let replacement = ActionLog::Replacement {
                        swap_action: repl.clone(),
                        updated_battlefield: updated_battlefield.clone(),
                    };
                    replacements.push(replacement);
                    if idx == pending_replacements.len() - 1 {
                        updated_battlefield.shift_ko_field_monsters()
                    } else {
                        updated_battlefield
                    }
                }
            );
        replacements
    }

    fn remove_pending_replacements_for_ko_monster(pending_replacements: &Vec<(MonsterId, Option<MonsterId>)>, knocked_out: MonsterId) -> Vec<(MonsterId, Option<MonsterId>)> {
        pending_replacements
            .iter()
            .map(|&(outgoing, incoming_opt)| 
                if outgoing == knocked_out { 
                    (outgoing, None) 
                } else { 
                    (outgoing, incoming_opt)
                })
            .collect()
    }

    fn remove_pending_actions_from_caster(pending_actions: &Vec<Action>, caster_id: MonsterId) -> Vec<Action> {
        pending_actions
            .iter()
            .filter_map(|action| (action.get_caster_id() != caster_id).then(|| action.clone()))
            .collect()
    }
    
}

#[derive(Clone)]
pub(super) enum OrchestratorStateEnum {
    ActionRegistration {
        pending_actions: Vec<Action>,
    },
    ReplacementRegistration {
        // TODO OrchestratorStateEnum::pending_replacements 
        // Split into two vecs: registered replacements (SwapAction) and expected replacements (MonsterId)

        // inv: item1 is the monster to replace, item2 is the client-registered replacement
        // inv: this list has a fixed size and is initialised by the orchestrator with a list of (item1: monster_id, item2: None)
        pending_replacements: Vec<(MonsterId, Option<MonsterId>)>,
    },
}

#[derive(Clone)]
pub(super) enum OrchestratorEvent {
    RegisterSwapAction {
        swap_action: SwapAction,
    },
    RegisterMoveAction {
        move_action: MoveAction,
    },
    UnregisterAction { 
        caster_id: MonsterId,
    },
    LockActionRegistrations,
    RegisterReplacement {
        swap_action: SwapAction,
    },
    UnregisterReplacement {
        knocked_out: MonsterId,
    },
    LockReplacementRegistrations,
}
