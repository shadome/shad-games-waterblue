use super::*;

#[derive(Clone)]
pub enum ActionLog {
    Replacement {
        swap_action: SwapAction,
        updated_battlefield: Battlefield,
    },
    Swap {
        action: SwapAction,
        updated_battlefield: Battlefield,
    },
    Move {
        // inv: exact match between Action::Move's targets and mitigations' targets
        action: MoveAction,
        mitigation: MitigationLog,
        updated_battlefield: Battlefield,
    },
}

pub trait HasUpdatedBattlefield {
    fn get_updated_battlefield(&self) -> &Battlefield;
}

impl HasUpdatedBattlefield for ActionLog {
    fn get_updated_battlefield(&self) -> &Battlefield {
        match self {
            ActionLog::Move { updated_battlefield, .. } => updated_battlefield,
            ActionLog::Swap { updated_battlefield, .. } => updated_battlefield,
            ActionLog::Replacement { updated_battlefield, .. } => updated_battlefield,
        }
    }
}

#[derive(Clone)]
pub struct LastTurnLog {
    initial_battlefield: Battlefield,
    actions: Vec<ActionLog>,
    required_replacements_player: Vec<MonsterId>,
    required_replacements_oppont: Vec<MonsterId>,
}

impl LastTurnLog {
    pub fn new(
        initial_battlefield: Battlefield,
        actions: Vec<ActionLog>, 
        required_replacements_player: Vec<MonsterId>,
        required_replacements_oppont: Vec<MonsterId>,
    ) -> Self {
        Self { initial_battlefield, actions, required_replacements_player, required_replacements_oppont }
    }

    pub fn set_final_battlefield(self, final_battlefield: Battlefield) -> Self {
        let mut _self = self;
        if !_self.actions.is_empty() {
            let last_idx = _self.actions.len() - 1;
            _self.actions[last_idx] = match _self.actions[last_idx].clone() {
                ActionLog::Move { action, mitigation, .. } => ActionLog::Move { action, mitigation, updated_battlefield: final_battlefield},
                ActionLog::Swap { action, .. } => ActionLog::Swap { action, updated_battlefield: final_battlefield },
                ActionLog::Replacement { swap_action, .. } => ActionLog::Replacement { swap_action, updated_battlefield: final_battlefield },
            };
        }
        _self
    }

    pub fn add_action_logs(self, actions: Vec<ActionLog>) -> Self {
        let mut _self = self;
        let mut actions = actions;
        _self.actions.append(&mut actions);
        _self
    }

    pub fn get_initial_battlefield(&self) -> &Battlefield { &self.initial_battlefield }
    pub fn get_final_battlefield(&self) -> &Battlefield { 
        self.actions.last().map(|last| last.get_updated_battlefield()).unwrap_or_else(|| &self.initial_battlefield)
    }
    pub fn get_actions(&self) -> &Vec<ActionLog> { &self.actions }
    pub fn get_required_replacements_player(&self) -> &Vec<MonsterId> { &self.required_replacements_player }
    pub fn get_required_replacements_oppont(&self) -> &Vec<MonsterId> { &self.required_replacements_oppont }
}

#[derive(Clone, Copy)]
pub struct MitigationLog {
    target: MonsterId,
    mitigation: Mitigation,
    effectiveness: Effectiveness,
    applied_status: Option<StatusCondition>,
    is_critical: bool,
    // True if the target has been knocked out because of this mitigation.
    is_lethal: bool,
}

impl MitigationLog {
    pub fn new(
        target: MonsterId,
        mitigation: Mitigation,
        effectiveness: Effectiveness,
        applied_status: Option<StatusCondition>,
        is_critical: bool,
        is_lethal: bool,
    ) -> MitigationLog {
        MitigationLog { target, mitigation, effectiveness, applied_status, is_critical, is_lethal }
    }

    pub fn get_target(&self) -> MonsterId { self.target }
    pub fn get_mitigation(&self) -> Mitigation { self.mitigation }
    pub fn get_effectiveness(&self) -> Effectiveness { self.effectiveness }
    pub fn get_applied_status(&self) -> Option<StatusCondition> { self.applied_status }
    pub fn is_critical(&self) -> bool { self.is_critical }
    pub fn is_lethal(&self) -> bool { self.is_lethal }
}
