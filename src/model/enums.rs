use super::*;

#[allow(dead_code)]
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum StatusCondition {
    Burn,
    Freeze,
    Infatuation,
    Paralysis,
    Poison,
    Toxic,
}

#[allow(dead_code)]
#[derive(Clone, Copy, Eq, PartialEq)]
pub enum Mitigation {
    Damage(u16),
    Healing(u16),
    Shield(u16),
}

#[allow(dead_code)]
#[derive(Clone, Copy, Eq, PartialEq)]
pub enum Effectiveness {
    Neutral,
    Good,
    Bad,
}

#[derive(Clone)]
pub enum Action {
    Swap(SwapAction),
    Move(MoveAction),
}

#[derive(Clone)]
pub struct SwapAction {
    incoming: MonsterId,
    outgoing: MonsterId,
}
impl SwapAction {
    pub fn new(incoming: MonsterId, outgoing: MonsterId) -> Self { 
        Self { incoming, outgoing } 
    }
    pub fn get_incoming(&self) -> MonsterId { self.incoming }
    pub fn get_outgoing(&self) -> MonsterId { self.outgoing }
}

#[derive(Clone)]
pub struct MoveAction {
    move_offset: u8,
    caster_id: MonsterId,
    // TODO target could be a POSITION and not a MONSTER ID 
    // since it could remain relevant even if the monster
    // initially targeted is replaced by another (e.g., swap)
    target_id: MonsterId,
}

impl MoveAction {
    pub fn new(move_offset: u8, caster_id: MonsterId, target_id: MonsterId) -> Self { 
        Self { move_offset, caster_id, target_id } 
    }
    pub fn get_caster_id(&self) -> MonsterId { self.caster_id }
    pub fn get_target_id(&self) -> MonsterId { self.target_id }
    pub fn get_move_offset(&self) -> u8 { self.move_offset }
}

pub trait HasCaster {
    fn get_caster_id(&self) -> MonsterId;
}

impl HasCaster for Action {
    fn get_caster_id(&self) -> MonsterId {
        match self {
            Action::Move(move_action) => move_action.get_caster_id(),
            Action::Swap(swap_action) => swap_action.get_outgoing(),
        }
    }
}
