use crate::model::*;

pub const fn calc_max_hp(
    level: Level,
    base_hpt: u8,
) -> u16 {
    // ({[IV+2*Base Stat+([EVs]/4)+100] * Level}/100)+10
    (((2 * base_hpt as u16) + 100) * level.as_u16() / 100) + 10
}