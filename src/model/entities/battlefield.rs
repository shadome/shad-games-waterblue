use uuid::Uuid;

use crate::model::*;

pub type MonsterId = Uuid;
pub type MoveIdx = u8;

pub const FIELD_MONSTERS_MAX_NB: u8 = 2;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Battlefield {
    // Note: not using HashMap because it doesn't ensure insertion order, esp. when getting .keys()/.values()
    // Note: not using Vec(key,val) either because it would require too many `.iter().map()` calls

    // inv: will never mutate
    // inv: arr.len() >= 1
    player_team: Vec<Monster>,
    // inv: will never mutate
    // inv: arr.len() >= 1
    oppont_team: Vec<Monster>,
    // inv: will never mutate
    // inv: arr.len() >= 1
    // inv: player_team_ids and oppont_team_ids contain unique ids
    // inv: player_team.len() == player_team_ids.len()
    // inv: player_team[i] == player_team_ids[i] are paired (by index)
    player_team_ids: Vec<MonsterId>,
    // inv: will never mutate
    // inv: arr.len() >= 1
    // inv: player_team_ids and oppont_team_ids contain unique ids 
    // inv: oppont_team.len() == oppont_team_ids.len()
    // inv: oppont_team[i] == oppont_team_ids[i] are paired (by index)
    oppont_team_ids: Vec<MonsterId>,

    // Ids of monsters currently on the field. Is always up to date.
    // inv: field_player_team.len() + party_player_team.len() == player_team_ids.len()
    // inv: arr.len() <= 2
    // inv: arr.[<any>].is_ko() == false
    field_player_monster_ids: Vec<MonsterId>,
    // inv: field_oppont_team.len() + party_oppont_team.len() == oppont_team_ids.len()
    // inv: arr.len() <= 2
    // inv: arr.[<any>].is_ko() == false
    field_oppont_monster_ids: Vec<MonsterId>,

    // Ids of monsters currently in the party. Is always up to date.
    // inv: field_player_team.len() + party_player_team.len() == player_team_ids.len()
    party_player_monster_ids: Vec<MonsterId>,
    // inv: field_oppont_team.len() + party_oppont_team.len() == oppont_team_ids.len()
    party_oppont_monster_ids: Vec<MonsterId>,
}



// pub enum Team {
//     Player,
//     Oppont,
// }

// impl std::ops::Index<Team> for Battlefield {
//     type Output = Vec<Monster>;
//     fn index(&self, team: Team) -> &Self::Output {
//         match team {
//             Team::Player => &self.player_team,
//             Team::Oppont => &self.oppont_team,
//         }
//     }
// }

// impl std::ops::Index<MonsterId> for Battlefield {
//     type Output = Monster;
//     fn index(&self, id: MonsterId) -> &Self::Output {
//         self.get_monster(id).unwrap()
//     }
// }

// create / read
impl Battlefield {

    pub const fn get_max_team_len() -> usize { 6 }
    
    pub fn new(
        player_ordered_monsters: Vec<Monster>,
        oppont_ordered_monsters: Vec<Monster>,
    ) -> Result<Battlefield, &'static str> {

        let max_team_len = Self::get_max_team_len();
        let len1 = player_ordered_monsters.len();
        let len2 = oppont_ordered_monsters.len();
        if len1 == 0 || len2 == 0 {
            Err("sequences should contain at least one element")
        } else if len1 > max_team_len || len2 > max_team_len {
            Err("sequences should contain at most six elements")
        } else {
            fn make_hashmap(vec: Vec<Monster>) -> Vec<(MonsterId, Monster)> {
                let mut res = Vec::new();
                for monster in vec {
                    res.push((Uuid::new_v4(), monster));
                }
                res
                // vec
                //     .iter()
                //     .map(|monster| (Uuid::new_v4(), *monster))
                //     .collect::<Vec<(MonsterId, Monster)>>()
            }
            let player_team = make_hashmap(player_ordered_monsters);
            let oppont_team = make_hashmap(oppont_ordered_monsters);

            fn take_first_2_ok(map: &Vec<(MonsterId, Monster)>) -> Vec<MonsterId> {
                map
                    .iter()
                    .filter_map(|(key, val)| (!val.is_ko()).then(|| *key))
                    .take(2)
                    .collect()
            }
            let field_player_monster_ids = take_first_2_ok(&player_team);
            let field_oppont_monster_ids = take_first_2_ok(&oppont_team);

            fn get_remaining_ids(map: &Vec<(MonsterId, Monster)>, excluded: &Vec<MonsterId>) -> Vec<MonsterId> {
                map
                    .iter()
                    .filter_map(|(id, _)| (!excluded.contains(id)).then(|| *id))
                    .collect()
            }
            let party_player_monster_ids = get_remaining_ids(&player_team, &field_player_monster_ids);
            let party_oppont_monster_ids = get_remaining_ids(&oppont_team, &field_oppont_monster_ids);

            let bf = Battlefield {
                player_team: player_team.iter().map(|(_, monster)| monster.clone()).collect(),
                oppont_team: oppont_team.iter().map(|(_, monster)| monster.clone()).collect(),
                player_team_ids: player_team.iter().map(|&(id, _)| id).collect(),
                oppont_team_ids: oppont_team.iter().map(|&(id, _)| id).collect(),
                field_player_monster_ids,
                field_oppont_monster_ids,
                party_player_monster_ids,
                party_oppont_monster_ids,
            };
            Ok(bf)
        }

    }

    // getters

    // TODO should be O(1)ish, use hash maps
    pub fn get_monster(&self, id: Uuid) -> Option<&Monster> { 
        if let Some(tried_player) = self.get_player_monster(id) { 
            Some(tried_player)
        } else if let Some(tried_oppont) = self.get_oppont_monster(id) {
            Some(tried_oppont)
        } else {
            None
        }
    }
    pub fn get_player_team(&self) -> Vec<&Monster> { 
        self.player_team.iter().map(|x| x).collect()
    }
    pub fn get_oppont_team(&self) -> Vec<&Monster> { 
        self.oppont_team.iter().map(|x| x).collect()
    }
    // TODO should be O(1)ish, use hash maps
    pub fn get_player_monster(&self, id: Uuid) -> Option<&Monster> { 
        let idx = self.player_team_ids
            .iter()
            .position(|&_id| _id == id)
        ;
        if let Some(idx) = idx {
            self.player_team.get(idx)
        } else {
            None
        }
    }
    // TODO should be O(1)ish, use hash maps
    pub fn get_oppont_monster(&self, id: Uuid) -> Option<&Monster> { 
        let idx = self.oppont_team_ids
            .iter()
            .position(|&_id| _id == id)
        ;
        if let Some(idx) = idx {
            self.oppont_team.get(idx)
        } else {
            None
        }
    }
    // TODO battlefield.rs: Return references to vectors, never vectors. Let the caller clone them if required.
    pub fn get_field_monster_ids(&self) -> Vec<MonsterId> {
        let mut res = Vec::new();
        res.extend(&self.field_player_monster_ids);
        res.extend(&self.field_oppont_monster_ids);
        res
            // .collect::<Vec<MonsterId>>()
    }
    pub fn get_field_player_monsters(&self) -> Vec<&Monster> {
        self.field_player_monster_ids
            .iter()
            .map(|&id| self.get_monster(id).unwrap())
            .collect()
    }
    pub fn get_field_oppont_monsters(&self) -> Vec<&Monster> {
        self.field_oppont_monster_ids
            .iter()
            .map(|&id| self.get_monster(id).unwrap())
            .collect()
    }
    pub fn get_party_player_monsters(&self) -> Vec<&Monster> {
        self.party_player_monster_ids
            .iter()
            .map(|&id| self.get_monster(id).unwrap())
            .collect()
    }
    pub fn get_party_oppont_monsters(&self) -> Vec<&Monster> {
        self.party_oppont_monster_ids
            .iter()
            .map(|&id| self.get_monster(id).unwrap())
            .collect()
    }

    pub fn get_monster_id(&self, monster: &Monster) -> Option<Uuid> {
        if let Some(tried_player) = self.get_player_monster_id(monster) { 
            Some(tried_player)
        } else if let Some(tried_oppont) = self.get_oppont_monster_id(monster) {
            Some(tried_oppont)
        } else {
            None
        }
    }
    pub fn get_player_monster_id(&self, monster: &Monster) -> Option<Uuid> {
        let idx = self.player_team
            .iter()
            .position(|_monster| _monster == monster)
        ;
        if let Some(idx) = idx {
            self.player_team_ids.get(idx).map(|&x| x)
        } else {
            None
        }
    }
    pub fn get_oppont_monster_id(&self, monster: &Monster) -> Option<Uuid> {
        let idx = self.oppont_team
            .iter()
            .position(|_monster| _monster == monster)
        ;
        if let Some(idx) = idx {
            self.oppont_team_ids.get(idx).map(|&x| x)
        } else {
            None
        }
    }

}

// write
impl Battlefield {

    /// Note: structural coherence is ensured within the battlefield, 
    /// i.e., monsters to swap are on the same team, etc.
    pub fn swap_monsters(
        self,
        field_id: MonsterId,
        party_id: MonsterId,
    ) -> Self {
        let mut _self = self;
        let is_players = _self.field_player_monster_ids.contains(&field_id);
        let field_ids = if is_players {
            &mut _self.field_player_monster_ids
        } else {
            &mut _self.field_oppont_monster_ids
        };
        let party_ids = if is_players {
            &mut _self.party_player_monster_ids
        } else {
            &mut _self.party_oppont_monster_ids
        };
        let field_idx = field_ids.iter().position(|&id| id == field_id);
        let party_idx = party_ids.iter().position(|&id| id == party_id);
        if let (Some(field_idx), Some(party_idx)) = (field_idx, party_idx) {
            std::mem::swap(&mut field_ids[field_idx], &mut party_ids[party_idx]);
        }
        _self
    }

    /// Note: structural coherence is ensured within the battlefield, 
    /// i.e., the cur_hpt has a relevant value (not < 0 etc.)
    /// Inv: does not swap monster, i.e., if the mitigated monster is knocked out,
    /// it remains on the battlefield with 0 hpt
    pub fn mitigate_monster(
        self, 
        target: MonsterId, 
        mitigation: Mitigation,
    ) -> Self {
        
        fn mitigate_monster_try_team(
            target_id: MonsterId, 
            mitigation: Mitigation,
            team: &mut Vec<Monster>,
            team_ids: &Vec<MonsterId>,
        ) -> bool {
            let team_pos = team_ids.iter().position(|&monster_id| monster_id == target_id);
            if let Some(team_pos) = team_pos {
                // TODO? is there a way to avoid cloning,
                // and for the battlefield to manipulate a vec of &mut Monster ?
                let monster = team[team_pos].clone();
                let new_hpt = match mitigation {
                    Mitigation::Damage(amount) => u16::checked_sub(monster.get_cur_hpt(), amount).unwrap_or(0),
                    _ => monster.get_cur_hpt(),
                };
                let _ = std::mem::replace(&mut team[team_pos], monster.set_cur_hpt(new_hpt));
                return true;
            }
            false
        }

        let mut _self = self;
        let _ = false
            || mitigate_monster_try_team(target, mitigation, &mut _self.player_team, &_self.player_team_ids)
            || mitigate_monster_try_team(target, mitigation, &mut _self.oppont_team, &_self.oppont_team_ids)
        ;
        _self
    }

    /// Moves the knocked out monsters which are still on the field to the party.
    /// Note that any replacement of a knocked out monster with another party monster
    /// must be handled with `swap_monsters` before calling this function.
    pub fn shift_ko_field_monsters(self) -> Self {
        
        fn shift_ko_field_monsters_for_team(
            team: &Vec<Monster>,
            team_ids: &Vec<MonsterId>,
            field_team_ids: &mut Vec<MonsterId>,
            party_team_ids: &mut Vec<MonsterId>,
        ) {
            let ko_monster_ids = team
                .iter()
                .enumerate()
                .filter_map(|(idx, monster)| monster.is_ko().then(|| team_ids[idx]))
                .collect::<std::collections::HashSet<_>>();
            let field_team_ids_set = field_team_ids.iter().map(|&id| id).collect::<std::collections::HashSet<_>>();
            let monster_ids_to_shift = ko_monster_ids
                .intersection(&field_team_ids_set);
            for &monster_id_to_shift in monster_ids_to_shift {
                field_team_ids.retain(|&monster_id| monster_id != monster_id_to_shift);
                party_team_ids.push(monster_id_to_shift);
            }
        }

        let mut _self = self;
        shift_ko_field_monsters_for_team(&_self.player_team, &_self.player_team_ids, &mut _self.field_player_monster_ids, &mut _self.party_player_monster_ids);
        shift_ko_field_monsters_for_team(&_self.oppont_team, &_self.oppont_team_ids, &mut _self.field_oppont_monster_ids, &mut _self.party_oppont_monster_ids);
        _self
    }

}