use crate::model::*;

mod level;
pub use level::*;

// Reflexions: use encaptulation for exposing only relevant fields of entities
// e.g., encapsulate Monster into a BattleMonster to only give access to
// fields relevant to the battlefield / battlecore logic
// e.g.2, encapsulate Monster into FoeMonster to hide information about player 2
// which should not be provided to player 1
// this could be done using traits (setting getters) or composition-structs
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Monster {
    // try not to use identifiers, at least inside base entities,
    // rather let the caller handle identifying underlying entities,
    // so that the collections and the collectees' identifiers are at the same place
    // and const fns, Copy etc. can be implemented in a maximum of cases
    // id: Uuid,
    species: Species,
    level: Level,
    status: Option<StatusCondition>,

    // using Specie's moves at the moment
    // moves: [Option<(Uuid, Move)>;4],

    cur_hpt: u16,
    max_hpt: u16,
}

impl Monster {
    pub const fn new<'a>(
        species: Species,
        level: Level,
    ) -> Monster {
        let max_hpt = formulas::calc_max_hp(
            level, 
            species.get_base_stats().get_hpt());
        Monster {
            species: species,
            level: level,
            status: None,
            cur_hpt: max_hpt,
            max_hpt: max_hpt,
        }
    }

    // getters
    
    pub const fn get_species(&self) -> Species { self.species }
    pub const fn get_status(&self) -> Option<StatusCondition> { self.status }
    pub const fn get_level(&self) -> Level { self.level }
    pub const fn get_cur_hpt(&self) -> u16 { self.cur_hpt }
    pub const fn get_max_hpt(&self) -> u16 { self.max_hpt }

    pub const fn is_ko(&self) -> bool { self.cur_hpt == 0 }

    // setters

    // TODO About mutable/set methods: should be Result<Self, &'static str> because some monsters will be immune to some statuses: should this logic be implemented here? maybe not! let's see with the ECS if a System is responsible for such things and if it is accessible here, else public the mutable methods for some specific battlecore modules only

    pub fn set_status(
        mut self, 
        status: Option<StatusCondition>,
    ) -> Self {
        self.status = status;
        self
    }

    pub fn set_cur_hpt_from_cur_hpt<F>(
        self,
        fnc: F,
    ) -> Self 
        where F: Fn(u16) -> u16 
    {
        let new_hpt = fnc(self.cur_hpt);
        let updated = self.set_cur_hpt(new_hpt);
        updated
    }

    pub fn set_cur_hpt_from_max_hpt<F>(
        self,
        fnc: F,
    ) -> Self 
        where F: Fn(u16) -> u16 
    {
        let new_hpt = fnc(self.max_hpt);
        let updated = self.set_cur_hpt(new_hpt);
        updated
    }

    pub fn set_cur_hpt(
        mut self,
        hpt: u16,
    ) -> Self {
        if hpt > self.max_hpt { 
            panic!("cur_hpt set to more than max_hpt");
        }
        self.cur_hpt = hpt;
        self
    }

}