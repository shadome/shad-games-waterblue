/// Example of strongly-typed field. 
/// TODO Every other Model field should follow this pattern, though this is not urgent.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Level { 
    raw: u8,
}

impl Level {

    pub const fn new(
        level: u8,
    ) -> Result<Level, &'static str> {
        match level {
            x if x < 1 => 
                Err("level should be strictly greater than 0"),
            x if x > 100  => 
                Err("level should be strictly lower than 101"),
            _ =>  
                Ok(Level { raw: level }),
        }
    }

    pub const fn as_u8(self) -> u8 { self.raw }
    pub const fn as_u16(self) -> u16 { self.raw as u16 }
}