use super::Effect;


#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Move {
    name: &'static str,
    effects: [Option<Effect>;4],
}

impl Move {
    // getters
    
    pub const fn get_name(&self) -> &'static str { self.name }

    pub fn get_effects(&self) -> Vec<Effect> {
        self.effects.iter().filter_map(|&effect| effect).collect()
    }
    // setters

}

impl Move {

    pub const ABSORB: Move = Move {
        name: "Absorb",
        effects: [Some(Effect::new_direct_damage(30)), None, None, None],
    };
    
    pub const CONFUSION: Move = Move {
        name: "Confusion",
        effects: [Some(Effect::new_direct_damage(150)), None, None, None],
    };
    
    pub const EMBER: Move = Move {
        name: "Ember",
        effects: [Some(Effect::new_direct_damage(40)), None, None, None],
    };
    
    pub const TACKLE: Move = Move {
        name: "Tackle",
        effects: [Some(Effect::new_direct_damage(45)), None, None, None],
    };
    
    pub const THUNDERSHOCK: Move = Move {
        name: "Thundershock",
        effects: [Some(Effect::new_direct_damage(40)), None, None, None],
    };
    
    pub const WATER_GUN: Move = Move {
        name: "Water gun",
        effects: [Some(Effect::new_direct_damage(40)), None, None, None],
    };
    
}