use super::*;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct Effect {
    name: &'static str,
    // inv: if arr[x] == None, arr[x+i] == None (i >= 0)
    components: [Option<EffectComponent>;4],
}

impl Effect {

    // pub const fn new(
    //     name: &'static str,
    //     components: [Option<EffectComponent>;4],
    // ) -> Effect {
    //     Effect { name, components }
    // }

    // getters
    
    pub const fn get_name(&self) -> &'static str { self.name }

    pub fn get_components(&self) -> Vec<&EffectComponent> {
        self.components
            .iter()
            .filter_map(|cmpnt| cmpnt.as_ref())
            .collect()
    }

    // setters

}

impl Effect {

    pub const fn new_direct_damage(power: u8) -> Effect {
        Effect {
            name: "direct damage",
            components: [
                Some(EffectComponent::Immediate),
                Some(EffectComponent::Damage(DamageComponent{ power })),
                None,
                None,
            ]
        }
    }
    
}