
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct DamageComponent { 
    pub power: u8,
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum EffectComponent {
    /// Effect triggers immediately
    Immediate,
    /// Effect deals damage
    Damage(DamageComponent),
}
