
// Copy is straight-forwardly required else this object cannot be returned in a const fn, and I like const fns
#[derive(Clone, Debug, Copy, Eq, PartialEq)]
pub struct Stats {
    // health points
    hpt: u8,
    // attack
    atk: u8,
    // defense
    def: u8,
    // special attack
    spa: u8,
    // special defense
    spd: u8,
    // speed
    spe: u8,
}

impl Stats {
    pub const fn new(
        hpt: u8, 
        atk: u8,
        def: u8,
        spa: u8,
        spd: u8,
        spe: u8,
    ) -> Self {
        Self {
            hpt: hpt,
            atk: atk,
            def: def,
            spa: spa,
            spd: spd,
            spe: spe,
        }
    }

    pub const fn get_hpt(self) -> u8 { self.hpt }
    pub const fn get_atk(self) -> u8 { self.atk }
    pub const fn get_def(self) -> u8 { self.def }
    pub const fn get_spa(self) -> u8 { self.spa }
    pub const fn get_spd(self) -> u8 { self.spd }
    pub const fn get_spe(self) -> u8 { self.spe }
}
