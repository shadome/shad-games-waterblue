use crate::model::*;

// Copy is straight-forwardly required else this object cannot be returned in a const fn, and I like const fns
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Species {
    nb: u16,
    name: &'static str,
    base_stats: Stats,
    moves: [Option<Move>;4],
}

impl Species {
    pub const fn get_nb(&self) -> u16 { self.nb }
    pub const fn get_name(&self) -> &'static str { self.name }
    pub const fn get_base_stats(&self) -> Stats { self.base_stats }

    pub fn get_moves(&self) -> Vec<Move> {
        self.moves
            .iter()
            .filter(|opt| !opt.is_none())
            .map(|opt| opt.unwrap())
            .collect()
    }
}

impl Species {

    pub const N001_BULBASAUR: Species = Species {
        nb: 1,
        name: "Bulbasaur",
        base_stats: Stats::new(045, 049, 049, 065, 065, 045),
        moves: [Some(Move::ABSORB), None, None, None],
    };

    pub const N004_CHARMANDER: Species = Species {
        nb: 4,
        name: "Charmander",
        base_stats: Stats::new(039, 052, 043, 060, 050, 065),
        moves: [Some(Move::EMBER), None, None, None],
    };

    pub const N007_SQUIRTLE: Species = Species {
        nb: 7,
        name: "Squirtle",
        base_stats: Stats::new(044, 048, 065, 050, 064, 043),
        moves: [Some(Move::WATER_GUN), None, None, None],
    };

    pub const N025_PIKACHU: Species = Species {
        nb: 25,
        name: "Pikachu",
        base_stats: Stats::new(035, 055, 040, 050, 050, 090),
        moves: [Some(Move::THUNDERSHOCK), None, None, None],
    };

    pub const N063_ABRA: Species = Species {
        nb: 63,
        name: "Abra",
        base_stats: Stats::new(025, 020, 015, 105, 055, 090),
        moves: [Some(Move::CONFUSION), None, None, None],
    };
    
    pub const N065_ALAKAZAM: Species = Species {
        nb: 65,
        name: "Alakazam",
        base_stats: Stats::new(055, 050, 045, 135, 095, 120),
        moves: [Some(Move::CONFUSION), None, None, None],
    };
    
    pub const N092_GASTLY: Species = Species {
        nb: 92,
        name: "Gastly",
        base_stats: Stats::new(030, 035, 030, 100, 035, 080),
        moves: [Some(Move::CONFUSION), None, None, None],
    };
    
    pub const N094_GENGAR: Species = Species {
        nb: 94,
        name: "Gengar",
        base_stats: Stats::new(060, 065, 060, 130, 075, 110),
        moves: [Some(Move::CONFUSION), None, None, None],
    };
    
}


