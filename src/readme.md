# Notes about syntax
This project's codebase implements what could be considered as peculiar syntaxes, which the following paragraphs try to motivate.
## Pipeline notation
Code, and computing in general, can often be broken down into an ordered set of instructions. Also, concurrent execution path in the flow of control are generally avoided as it produces code harder to understand / maintain for the human and harder to optimise for the compiler. This codebase attemps to leverage this fact by maximising the use of Rust's support for pattern matching when concurrent execution paths are required, and with pipeline notations when they are not.
### Typical example
```rust
// chaining iterables in a pipeline
let my_pipeline_result_1 = my_existing_vec // (1)
    .iter() // (2)
    .map(data_transformer) // (2)
    .filter(data_filterer) // (2)
    .collect() // (2)
; // (3)
// chaining a builder design pattern
let my_pipeline_result_2 = MyClass // (1)
    ::new(arg1, arg2) // (2)
    .add_something(optional_arg1) // (2)
    .modify_something(optional_arg2) // (2)
; // (3)
// processing an Option<T> with native chaining support
let my_pipeline_result_3 = my_optional_value // (1)
    .map(data_transformer) // (2)
    .filter(data_filterer) // (2)
    .or_else(default_initialiser) // (2)
; // (3)
// processing a boolean pipeline (as a conjunctive normal form (CNF), could also be a DNF)
let my_pipeline_result_4 = true // (1)
    && condition1() // (2)
    && (condition2() || condition3()) // (2)
    && condition4() // (2)
; // (3)
```
Where:
1. is the initial statement which does not perform any sort of treatment,
2. is a pipeline step, *i.e.*, a step transforming the previous statement or temporary value in a *chained* fashion, and
3. is the end of the pipeline block, (optionnaly) on a separate line to be more explicit *(and incidentally facilitate cut/paste operations)*.
### Notes 
Note that vertical space should never be an issue resolved by casually inlining some code. In case some part of the code becomes less readable due to too much vertical space being required, functions involved in the treatment should be grouped in new functions with increased abstraction, thus reducing the number of steps.

Note that pipeline notations sometimes allow for pipeline failure, in which case it is important for costly treatments to be lazily computed (using closures, lazy boolean operators, etc.). Failures include: 
* a boolean pipeline which fails at a certain condition,
* an Option pipeline which produces None at some point,
* an iterable pipeline which filters down to Empty at some point,
* etc.

Proper support for method chaining should eventually be included, e.g., https://github.com/InquisitivePenguin/cascade.