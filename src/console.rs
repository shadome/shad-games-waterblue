use crossterm::{
    event::{*, self},
    terminal::{disable_raw_mode, enable_raw_mode},
};
use std::{*, error::Error, sync::mpsc::{self, *}, time::*};
use tui::{*, backend::*};

mod layout;
mod utils;
mod finite_state_machines;
mod components;
mod dependencies;

use finite_state_machines::*;
use utils::*;

use crate::model::Battlefield;

use self::dependencies::*;


/// TODO 220924
/// - merge all kind of logs including replacements
/// - remove separate states for battlefield rendering
/// TODO 220920
/// - make a common struct for logs, be it end of turn or replacement
///    - also because in the future, replacing could trigger damage, etc.
///    - and then as long as the last_turn_log's FSM refreshes its internal state, keep going 
///      (update the diagrams: lock replacements should refresh the internal state of the already running FSM)
/// 

pub(super) fn run(battlefield: &Battlefield) -> Result<(), Box<dyn Error>> {
    
    enable_raw_mode().expect("can run in raw mode");
    
    let rx = run_console_evt_listening_thread();
    let mut terminal = Terminal::new(CrosstermBackend::new(io::stdout()))?;
    // The current state is updated in this scope when the user hits an action key
    let mut cur_state_user_choice = UserChoiceState::start(battlefield.clone());
    let mut cur_state_end_of_turn_logs: Option<EndOfTurnState<_,_>> = None;
    // Rendering params
    let mut battlefield = cur_state_user_choice.battlefield();
    let mut battlelog_msg = cur_state_user_choice.battlelog_msg();
    let mut ordered_choice_labels = cur_state_user_choice.ordered_choice_labels();
    // Event instances are constantly mapped (key is list_state.selected index) by the renderer
    // so when an action key is hit, the current event can be retrieved to transition the FSM
    let mut actions_state = ActionListState::default();
    terminal.clear()?;
    actions_state.list_state.select(Some(0));
    loop {
        terminal.draw(|frame| {
            layout::render(
                frame, 
                &mut actions_state,
                battlefield,
                battlelog_msg, 
                ordered_choice_labels,
            );
        })?;
        match rx.recv()? {
            ConsoleEvent::Input(event) => match event.code {
                KeyCode::Char('q') => {
                    disable_raw_mode()?;
                    terminal.clear()?;
                    terminal.show_cursor()?;
                    break;
                },
                KeyCode::Up => {
                    actions_state.select_prev();
                },
                KeyCode::Down => {
                    actions_state.select_next();
                },
                KeyCode::Right => {
                    let cur_choice_idx = actions_state.list_state.selected().unwrap();
                    // user choice FSM has priority
                    if !cur_state_user_choice.is_done() {
                        let event = cur_state_user_choice.ordered_choice_events()[cur_choice_idx].clone();
                        cur_state_user_choice = cur_state_user_choice.do_transition(event);
                        if !cur_state_user_choice.is_done() {
                            // if the above do_transition was a transition towards a valid state,
                            // update the display variables from the new state
                            battlefield = cur_state_user_choice.battlefield();
                            battlelog_msg = cur_state_user_choice.battlelog_msg();
                            ordered_choice_labels = cur_state_user_choice.ordered_choice_labels();
                        } else {
                            // if the above do_transition was a transition towards Done,
                            // start the second FSM and update the display variables from this second FSM
                            let end_of_turn_logs = cur_state_user_choice.try_get_last_turn_log().unwrap().clone();
                            cur_state_end_of_turn_logs = Some(
                                EndOfTurnState::start(end_of_turn_logs, RobotAI::new(), BackEndApi::new())
                            );
                            battlefield = cur_state_end_of_turn_logs.as_ref().unwrap().battlefield();
                            battlelog_msg = cur_state_end_of_turn_logs.as_ref().unwrap().battlelog_msg();
                            ordered_choice_labels = cur_state_end_of_turn_logs.as_ref().unwrap().ordered_choice_labels();
                        }
                    } else {
                        // inv: at this point, the second FSM is neither None nor Done
                        let event = cur_state_end_of_turn_logs.as_ref().unwrap().ordered_choice_events()[cur_choice_idx].clone();
                        cur_state_end_of_turn_logs = Some(cur_state_end_of_turn_logs.unwrap().do_transition(event));
                        // the battlefield must be updated whether the above do_transition was a transition towards Done or a valid state
                        battlefield = cur_state_end_of_turn_logs.as_ref().unwrap().battlefield();
                        if !cur_state_end_of_turn_logs.as_ref().unwrap().is_done() {
                            // if the above do_transition was a transition towards a valid state,
                            // update the display variables from the new state
                            battlelog_msg = cur_state_end_of_turn_logs.as_ref().unwrap().battlelog_msg();
                            ordered_choice_labels = cur_state_end_of_turn_logs.as_ref().unwrap().ordered_choice_labels();
                        } else {
                            // if the above do_transition was a transition towards Done,
                            // restart the first FSM and update the display variables from this first FSM
                            cur_state_user_choice = UserChoiceState::start(battlefield.clone());
                            battlefield = cur_state_user_choice.battlefield();
                            battlelog_msg = cur_state_user_choice.battlelog_msg();
                            ordered_choice_labels = cur_state_user_choice.ordered_choice_labels();
                        }
                    }
                    actions_state.list_state.select(Some(0));
                }
                KeyCode::Left => {
                    // Note that this implementation is simplified because KeyCode::Left => Event::GoBack
                    // and Event::GoBack cannot imply that the next state is Done for any of the two FSM
                    if !cur_state_user_choice.is_done() {
                        cur_state_user_choice = cur_state_user_choice.do_transition(UserChoiceEvent::GoBack);
                        battlefield = cur_state_user_choice.battlefield();
                        battlelog_msg = cur_state_user_choice.battlelog_msg();
                        ordered_choice_labels = cur_state_user_choice.ordered_choice_labels();
                    } else if cur_state_end_of_turn_logs.is_some() && !cur_state_end_of_turn_logs.as_ref().unwrap().is_done() {
                        cur_state_end_of_turn_logs = Some(cur_state_end_of_turn_logs.unwrap().do_transition(EndOfTurnEvent::GoBack));
                        battlefield = cur_state_end_of_turn_logs.as_ref().unwrap().battlefield();
                        battlelog_msg = cur_state_end_of_turn_logs.as_ref().unwrap().battlelog_msg();
                        ordered_choice_labels = cur_state_end_of_turn_logs.as_ref().unwrap().ordered_choice_labels();
                    }
                    actions_state.list_state.select(Some(0));
                }
                _ => {}
            },
            ConsoleEvent::Tick => {}
        }
    }
    Ok(())
}

// private

enum ConsoleEvent<I> {
    Input(I),
    Tick,
}

fn run_console_evt_listening_thread() -> Receiver<ConsoleEvent<KeyEvent>> {
    let (tx, rx) = mpsc::channel();
    let tick_rate = Duration::from_millis(200);
    thread::spawn(move || {
        let mut last_tick = Instant::now();
        loop {
            let timeout = tick_rate
                .checked_sub(last_tick.elapsed())
                .unwrap_or_else(|| Duration::from_secs(0));
            // `poll` also serves as a Sleep which returns true immediately 
            // when an event occurs or sleeps for `timeout: Duration` otherwise
            if event::poll(timeout).expect("poll works") {
                if let Event::Key(key) = event::read().expect("can read events") {
                    tx.send(ConsoleEvent::Input(key)).expect("can send events");
                }
            }
            if last_tick.elapsed() >= tick_rate {
                if let Ok(_) = tx.send(ConsoleEvent::Tick) {
                    last_tick = Instant::now();
                }
            }
        }
    });
    rx
}


#[cfg(test)]
mod console_test_utils {
    use crate::model::{Battlefield, Monster, MonsterId, Action, SwapAction, LastTurnLog, MoveAction};

    use super::dependencies::{AIDependencyTrait, BackEndDependencyTrait};


    pub struct TestAI<FnGetAction, FnGetRepl> where 
        FnGetAction: Fn(&Battlefield, &Monster, &Vec<MonsterId>, &Vec<MonsterId>) -> Option<Action>,
        FnGetRepl: Fn(&Battlefield, &MonsterId, &Vec<MonsterId>) -> SwapAction,
    {
        get_action: FnGetAction,
        get_replacement: FnGetRepl,
    }

    impl<FnGetAction, FnGetRepl> TestAI<FnGetAction, FnGetRepl> where 
        FnGetAction: Fn(&Battlefield, &Monster, &Vec<MonsterId>, &Vec<MonsterId>) -> Option<Action>,
        FnGetRepl: Fn(&Battlefield, &MonsterId, &Vec<MonsterId>) -> SwapAction,
    {
        pub fn new(get_action: FnGetAction, get_replacement: FnGetRepl) -> Self { 
            Self { get_action, get_replacement } 
        }
    }
    
    impl<FnGetAction, FnGetRepl> AIDependencyTrait for TestAI<FnGetAction, FnGetRepl> where 
        FnGetAction: Fn(&Battlefield, &Monster, &Vec<MonsterId>, &Vec<MonsterId>) -> Option<Action>,
        FnGetRepl: Fn(&Battlefield, &MonsterId, &Vec<MonsterId>) -> SwapAction,
    {
        
        fn get_action(&self, bf: &Battlefield, req_caster: &Monster, eligible_incomings: &Vec<MonsterId>, eligible_move_targets: &Vec<MonsterId>) -> Option<Action> {
            (self.get_action)(bf, req_caster, eligible_incomings, eligible_move_targets)
        }
    
        fn get_replacement(&self, bf: &Battlefield, req_replacement: &MonsterId, eligible_replacements: &Vec<MonsterId>) -> SwapAction {
            (self.get_replacement)(bf, req_replacement, eligible_replacements)
        }
    
    }
    
    pub struct TestBackEndApi<FnStart, FnRegMove, FnRegSwap, FnRegRepl, FnLockActionReg, FnLockReplReg, FnUnregAction, FnUnregRepl, FnGetEliSwapInc, FnGetEliMoveTar> where
        FnStart: Fn(Battlefield),
        FnRegMove: Fn(MoveAction),
        FnRegSwap: Fn(SwapAction),
        FnRegRepl: Fn(SwapAction),
        FnLockActionReg: Fn() -> LastTurnLog,
        FnLockReplReg: Fn() -> LastTurnLog,
        FnUnregAction: Fn(MonsterId),
        FnUnregRepl: Fn(MonsterId),
        FnGetEliSwapInc: Fn(MonsterId) -> Vec<MonsterId>,
        FnGetEliMoveTar: Fn(MonsterId, u8) -> Vec<MonsterId>,
    {
        // start_battle: Box<dyn FnMut(Battlefield) + 'static>,
        start_battle: FnStart,
        start_battle_cnt: usize,
        register_monster_move: FnRegMove,
        register_monster_move_cnt: usize,
        register_monster_swap: FnRegSwap,
        register_monster_swap_cnt: usize,
        register_replacement: FnRegRepl,
        register_replacement_cnt: usize,
        lock_action_reg: FnLockActionReg,
        lock_action_reg_cnt: usize,
        lock_repl_reg: FnLockReplReg,
        lock_repl_reg_cnt: usize,
        unregister_monster_action: FnUnregAction,
        unregister_monster_action_cnt: usize,
        unregister_replacement: FnUnregRepl,
        unregister_replacement_cnt: usize,
        get_eligible_swap_incoming_monsters: FnGetEliSwapInc,
        get_eligible_swap_incoming_monsters_cnt: usize,
        get_eligible_move_target_monsters: FnGetEliMoveTar,
        get_eligible_move_target_monsters_cnt: usize,
    }

    impl<FnStart, FnRegMove, FnRegSwap, FnRegRepl, FnLockActionReg, FnLockReplReg, FnUnregAction, FnUnregRepl, FnGetEliSwapInc, FnGetEliMoveTar> 
        TestBackEndApi<FnStart, FnRegMove, FnRegSwap, FnRegRepl, FnLockActionReg, FnLockReplReg, FnUnregAction, FnUnregRepl, FnGetEliSwapInc, FnGetEliMoveTar>
    where
        FnStart: Fn(Battlefield),
        FnRegMove: Fn(MoveAction),
        FnRegSwap: Fn(SwapAction),
        FnRegRepl: Fn(SwapAction),
        FnLockActionReg: Fn() -> LastTurnLog,
        FnLockReplReg: Fn() -> LastTurnLog,
        FnUnregAction: Fn(MonsterId),
        FnUnregRepl: Fn(MonsterId),
        FnGetEliSwapInc: Fn(MonsterId) -> Vec<MonsterId>,
        FnGetEliMoveTar: Fn(MonsterId, u8) -> Vec<MonsterId>,
    {
        pub fn new(
            // mut test: Box<(dyn FnMut(Battlefield) + 'static)> ,
            start_battle: FnStart,
            register_monster_move: FnRegMove,
            register_monster_swap: FnRegSwap,
            register_replacement: FnRegRepl,
            lock_action_reg: FnLockActionReg,
            lock_repl_reg: FnLockReplReg,
            unregister_monster_action: FnUnregAction,
            unregister_replacement: FnUnregRepl,
            get_eligible_swap_incoming_monsters: FnGetEliSwapInc,
            get_eligible_move_target_monsters: FnGetEliMoveTar,
        ) -> Self {
            // let start_battle = |_| {};
            Self { 
                // start_battle: Box::new(|bf| (test)(bf)),
                // start_battle: Box::new(|_| {}),
                start_battle,                               lock_action_reg,
                lock_repl_reg,                              unregister_monster_action,  
                register_monster_move,                      register_monster_swap,
                register_replacement,                       unregister_replacement, 
                get_eligible_swap_incoming_monsters,        get_eligible_move_target_monsters,
                start_battle_cnt: 0,                        lock_action_reg_cnt: 0,
                lock_repl_reg_cnt: 0,                       unregister_monster_action_cnt: 0,  
                register_monster_move_cnt: 0,               register_monster_swap_cnt: 0,
                register_replacement_cnt: 0,                unregister_replacement_cnt: 0, 
                get_eligible_swap_incoming_monsters_cnt: 0, get_eligible_move_target_monsters_cnt: 0,
            } 
        }
        pub fn start_battle_cnt(&self) -> usize { self.start_battle_cnt }
        pub fn lock_action_reg_cnt(&self) -> usize { self.lock_action_reg_cnt }
        pub fn lock_repl_reg_cnt(&self) -> usize { self.lock_repl_reg_cnt }
        pub fn unregister_monster_action_cnt(&self) -> usize { self.unregister_monster_action_cnt }
        pub fn register_monster_move_cnt(&self) -> usize { self.register_monster_move_cnt }
        pub fn register_monster_swap_cnt(&self) -> usize { self.register_monster_swap_cnt }
        pub fn register_replacement_cnt(&self) -> usize { self.register_replacement_cnt }
        pub fn unregister_replacement_cnt(&self) -> usize { self.unregister_replacement_cnt }
        pub fn get_eligible_swap_incoming_monsters_cnt(&self) -> usize { self.get_eligible_swap_incoming_monsters_cnt }
        pub fn get_eligible_move_target_monsters_cnt(&self) -> usize { self.get_eligible_move_target_monsters_cnt }

    }

    impl<FnStart, FnRegMove, FnRegSwap, FnRegRepl, FnLockActionReg, FnLockReplReg, FnUnregAction, FnUnregRepl, FnGetEliSwapInc, FnGetEliMoveTar> 
        BackEndDependencyTrait for TestBackEndApi<FnStart, FnRegMove, FnRegSwap, FnRegRepl, FnLockActionReg, FnLockReplReg, FnUnregAction, FnUnregRepl, FnGetEliSwapInc, FnGetEliMoveTar>
    where 
        FnStart: Fn(Battlefield),
        FnRegMove: Fn(MoveAction),
        FnRegSwap: Fn(SwapAction),
        FnRegRepl: Fn(SwapAction),
        FnLockActionReg: Fn() -> LastTurnLog,
        FnLockReplReg: Fn() -> LastTurnLog,
        FnUnregAction: Fn(MonsterId),
        FnUnregRepl: Fn(MonsterId),
        FnGetEliSwapInc: Fn(MonsterId) -> Vec<MonsterId>,
        FnGetEliMoveTar: Fn(MonsterId, u8) -> Vec<MonsterId>,
    {
        fn start_battle(&mut self, battlefield: Battlefield) { 
            self.start_battle_cnt += 1;
            (self.start_battle)(battlefield)
        }

        fn unregister_monster_action(&mut self, monster_id: MonsterId) { 
            self.unregister_monster_action_cnt += 1;
            (self.unregister_monster_action)(monster_id) 
        }

        fn register_monster_move(&mut self, move_action: MoveAction) { 
            self.register_monster_move_cnt += 1;
            (self.register_monster_move)(move_action) 
        }

        fn register_monster_swap(&mut self, swap_action: SwapAction) { 
            self.register_monster_swap_cnt += 1;
            (self.register_monster_swap)(swap_action) 
        }

        fn lock_action_registrations(&mut self) -> LastTurnLog { 
            self.lock_action_reg_cnt += 1;
            (self.lock_action_reg)() 
        }

        fn register_replacement(&mut self, swap_action: SwapAction) { 
            self.register_replacement_cnt += 1;
            (self.register_replacement)(swap_action) 
        }

        fn unregister_replacement(&mut self, outgoing_id: MonsterId) { 
            self.unregister_replacement_cnt += 1;
            (self.unregister_replacement)(outgoing_id) 
        }

        fn lock_replacement_registrations(&mut self) -> LastTurnLog { 
            self.lock_repl_reg_cnt += 1;
            (self.lock_repl_reg)() 
        }

        fn get_eligible_swap_incoming_monsters(&mut self, outgoing: MonsterId) -> Vec<MonsterId> { 
            self.get_eligible_swap_incoming_monsters_cnt += 1;
            (self.get_eligible_swap_incoming_monsters)(outgoing) 
        }

        fn get_eligible_move_target_monsters(&mut self, caster: MonsterId, move_offset: u8) -> Vec<MonsterId> { 
            self.get_eligible_move_target_monsters_cnt += 1;
            (self.get_eligible_move_target_monsters)(caster, move_offset) 
        }

    }
}

#[cfg(test)]
mod console_test {
    use crate::*;
    use super::*;
 
    #[test]
    fn test_unfold_swap_log_double_replacement_required() {
        // setup

        let level = Level::new(50).unwrap();
        let player_monsters = vec![
            Monster::new(Species::N001_BULBASAUR, level).set_cur_hpt(0),
            Monster::new(Species::N001_BULBASAUR, level).set_cur_hpt(0),
            Monster::new(Species::N001_BULBASAUR, level),
            Monster::new(Species::N001_BULBASAUR, level),
        ];
        let oppont_monsters = vec![
            Monster::new(Species::N007_SQUIRTLE, level),
            Monster::new(Species::N007_SQUIRTLE, level),
            Monster::new(Species::N007_SQUIRTLE, level),
            Monster::new(Species::N007_SQUIRTLE, level),
        ];
        let bf = Battlefield::new(player_monsters, oppont_monsters).unwrap();
        let initial_bf = bf.clone();
        let monster_field_id_1 = bf.get_monster_id(bf.get_field_player_monsters()[0]).unwrap();
        let monster_field_id_2 = bf.get_monster_id(bf.get_field_player_monsters()[1]).unwrap();
        let monster_party_id_1 = bf.get_monster_id(bf.get_field_player_monsters()[0]).unwrap();
        let monster_party_id_2 = bf.get_monster_id(bf.get_field_player_monsters()[1]).unwrap();
        let last_turn_log = LastTurnLog::new(bf.clone(), Vec::new(), vec![monster_field_id_1, monster_field_id_2], Vec::new());

        let backend_api = BackEndApi::new(); // TODO
        let robot_ai = RobotAI::new(); // TODO

        // let l1 = initial_bf[Team::Player].clone();
        // let l2 = &initial_bf[monster_field_id_1];
        // test
        orchestrator::start_battle(bf);
        let result_bf = EndOfTurnState::start(last_turn_log, robot_ai, backend_api)
            // Pop next action log => Pop next required replacement
            .do_transition(EndOfTurnEvent::GoNext)
            // TR-02
            .do_transition(EndOfTurnEvent::GoNext)
            // TR-41
            .do_transition(EndOfTurnEvent::ReplacementChoice { incoming_id: monster_party_id_1 })
            // TR-02
            .do_transition(EndOfTurnEvent::GoNext)
            // TR-41
            .do_transition(EndOfTurnEvent::ReplacementChoice { incoming_id: monster_party_id_2 })
            
        ;

        // assert
        // assert_eq!(initial_bf, bf);
    }

}
